//  LTUIManager.m
//  Created by Fabio Teles on 5/19/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTUIManager.h"
#import "LTViewController.h"
#import "LTMenuController.h"
#import "LTPopupViewController.h"

#import "LTOperationQueue.h"
#import "LTAlertOperation.h"
#import "LTBlockOperation.h"

#import "LTBlockCondition.h"
#import "LTMutuallyExclusiveCondition.h"

#import "LTInitSequenceOperation.h"
#if ENABLE_MARKETPLACE
#import "LTRedemptionSequenceOperation.h"
#endif

#if ENABLE_ACHIEVEMENT
#import "LTAchievementSequenceOperation.h"
#import "LTAchievement.h"
#import "LTUIManager+Achievement.h"
#if ENABLE_INTERSTITIAL
#import "LTUIManager+Interstitial.h"
#import "LTVideoInterstitialSequenceOperation.h"
#endif
#endif


#import "LTNetworkManager.h"

#import "LTApp.h"
#import "LTSettings.h"
#import "LTRecievedPointsData.h"
#import "LTColor.h"
#import "LTData.h"

#import "LTConstants.h"
#import "LTMWLogging.h"
#import "LTTools.h"

#import "LTPPRevealSideViewController.h"

NSString * LTMainSceneIdentifier            = @"MainScene";
NSString * LTMenuSceneIdentifier            = @"MenuScene";

NSString * LTTermsSegueIdentifier           = @"TermsSegue";
NSString * LTAboutSegueIdentifier           = @"AboutSegue";

NSString * LTUIStoryboardName               = @"LTUserInterface";

NSString * const LTDismissedAlertKey        = @"com.lootsie.ui.manager.dismissedAlert";

@interface LTUIManager ()

@property (nonatomic, strong) UIViewController *wrapRootViewController;
@property (nonatomic, strong) LTPPRevealSideViewController *rootViewController;
@property (nonatomic, strong) UIStoryboard *resourcesStoryboard;

@end

@implementation LTUIManager

- (instancetype)init {
    if (self = [super init]) {
        _ianBehavior = LTIANBehaviorGoToRewards;
        _renderingMode = LTRenderingModeAutomatic;
        _onlyShowAlertsWhenUIOpen = YES;
#if ENABLE_ACHIEVEMENT
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleAchievementReached:) name:LTAchievementReachedNotification object:self];
#if ENABLE_INTERSTITIAL
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleWatchedVideoInterstitial:) name:LTVideoInterstitialWathcedNotification object:self];
#endif
#endif
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Properties
- (UIStoryboard *)resourcesStoryboard {
	if (!_resourcesStoryboard) {
        NSBundle *bundle = [NSBundle bundleForClass:[self class]];
		_resourcesStoryboard = [UIStoryboard storyboardWithName:LTUIStoryboardName bundle:bundle];
	}
	return _resourcesStoryboard;
}

- (UIViewController *)wrapRootViewController {
    if (!_wrapRootViewController) {
        _wrapRootViewController = [[UIViewController alloc] init];
        _wrapRootViewController.view.backgroundColor = [UIColor clearColor];
        _wrapRootViewController.modalPresentationStyle = UIModalPresentationCustom;
        _wrapRootViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    }
    return _wrapRootViewController;
}

- (LTPPRevealSideViewController *)rootViewController {
	if (!_rootViewController) {
        UIViewController *mainController = [self.resourcesStoryboard instantiateViewControllerWithIdentifier:LTMainSceneIdentifier];
        UIViewController *menuViewController = [self.resourcesStoryboard instantiateViewControllerWithIdentifier:LTMenuSceneIdentifier];
		_rootViewController = [[LTPPRevealSideViewController alloc] initWithRootViewController:mainController];
		[_rootViewController setOption:(LTPPRevealSideOptionsiOS7StatusBarFading | LTPPRevealSideOptionsKeepOffsetOnRotation)];
		_rootViewController.fakeiOS7StatusBarColor = [UIColor whiteColor];
		[_rootViewController preloadViewController:menuViewController forSide:LTPPRevealSideDirectionRight];
	}
	return _rootViewController;
}

- (LTMenuController *)menuController {
    LTMenuController *controller = (LTMenuController *)[self.rootViewController controllerForSide:LTPPRevealSideDirectionRight];
	return controller;
}

- (LTViewController *)mainController {
    return (LTViewController *)self.rootViewController.rootViewController;
}

#pragma mark - Pages UI
- (void)showPageWithSegueIdentifier:(NSString *)identifier {
    if (self.status == LTSDKStatusFailed) {
        LTLogCritical(@"Lootsie's SDK has failed to initialize");
        return;
    }
    
	if (!self.mainController.currentViewController) {
        UIViewController *firstPage = [self.resourcesStoryboard instantiateViewControllerWithIdentifier:[identifier stringByReplacingOccurrencesOfString:@"Segue" withString:@"Page"]];
		[self.mainController setFirstPage:firstPage identifier:identifier];
	} else if ([self.mainController shouldPerformSegueWithIdentifier:identifier sender:self]) {
		[self.mainController performSegueWithIdentifier:identifier sender:self];
	}

	if (![self isShowingUI]) {
        // wraps showing the UI in a operation so we can make it mutually exclusive
        LTBlockOperation *showUIOP = [[LTBlockOperation alloc] initWithMainQueueBlock:^{
            [self willShowLootsieUI:^{
                [self.wrapRootViewController presentViewController:self.rootViewController animated:YES completion:nil];
            }];
        }];
        [showUIOP setSafeName:@"com.lootsie.ui.showUI"];
        showUIOP.userInitiated = YES;
        [showUIOP addCondition:[LTMutuallyExclusiveCondition viewControllerMutuallyExclusive]];
        [self->_generalQueue addOperation:showUIOP];
	}
}

- (void)showTermsPage {
	[self showPageWithSegueIdentifier:LTTermsSegueIdentifier];
}

- (void)showAboutPage {
	[self showPageWithSegueIdentifier:LTAboutSegueIdentifier];
}

- (BOOL)isShowingUI {
	return (self.wrapRootViewController && nil != self.wrapRootViewController.presentingViewController);
}

- (void)closeUI {
	if ([self isShowingUI]) {
        // wraps showing the UI in a operation so we can make it mutually exclusive
        __weak __typeof(self) weakSelf = self;
        LTBlockOperation *closeUIOP = [[LTBlockOperation alloc] initWithMainQueueBlock:^{
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            BOOL  isAnimated = self.wrapRootViewController.presentedViewController != nil;
            [self.wrapRootViewController dismissViewControllerAnimated:isAnimated completion:^{
                // notify delegate of closing UI
                if ([strongSelf.delegate respondsToSelector:@selector(lootsieUIManagerDidCloseUI)]) {
                    [strongSelf.delegate lootsieUIManagerDidCloseUI];
                }
                [self.wrapRootViewController.presentingViewController dismissViewControllerAnimated:NO completion:nil];
            }];
        }];
        [closeUIOP setSafeName:@"com.lootsie.ui.closeUI"];
        closeUIOP.userInitiated = YES;
        [closeUIOP addCondition:[LTMutuallyExclusiveCondition viewControllerMutuallyExclusive]];
        [self->_generalQueue addOperation:closeUIOP];
	}
}

#pragma mark - Alert
- (void)showAlert:(LTAlertOperation *)alert {
    [self showAlert:alert ignoreIfBlockedByAnother:NO];
}

- (void)showAlert:(LTAlertOperation *)alert ignoreIfBlockedByAnother:(BOOL)ignoreIfBlockedByAnother {
    __weak __typeof(alert) weakAlert = alert;
    [alert addCondition:[[LTBlockCondition alloc] initWithConditionBlock:^BOOL(LTOperation *operation, NSDictionary *__autoreleasing *errorUserInfo) {
        if (self.onlyShowAlertsWhenUIOpen && self.isShowingUI) {
            return YES;
        } else {
            *errorUserInfo = @{NSLocalizedDescriptionKey: NSLocalizedStringFromTable(@"Alert dismissed!", @"LTLootsie", nil),
                               NSLocalizedRecoverySuggestionErrorKey: NSLocalizedStringFromTable(@"Lootsie's UI is closed, therefore the alert was dismissed to not interrupt your app's current state", @"LTLootsie", nil),
                               LTDismissedAlertKey: weakAlert};
            return NO;
        }
    }]];
    [self->_generalQueue addOperation:alert];
    
        // No mutually exclusive blockage for alerts (Do not display duplicate error alerts)
        if (ignoreIfBlockedByAnother) {
        [alert.dependencies enumerateObjectsUsingBlock:^(NSOperation * _Nonnull dependency, NSUInteger idx, BOOL * _Nonnull stop) {
            if ([dependency isKindOfClass:[LTAlertOperation class]] && dependency.isExecuting) {
                [alert cancel];
                *stop = YES;
            }
        }];
    }
}
- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message {
    [self showAlertWithTitle:title message:message ignoreIfBlockedByAnother:NO];
}

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message ignoreIfBlockedByAnother:(BOOL)ignoreIfBlockedByAnother {
    __weak __typeof(self) weakSelf = self;
    LTAlertOperation *alertOp = [[LTAlertOperation alloc] initWithTitle:[title copy]
                                                                message:[message copy]
                                             dynamicPresentationContext:^UIViewController *{
                                                 return [weakSelf presentationContext];
                                             }];
    [self showAlert:alertOp ignoreIfBlockedByAnother:ignoreIfBlockedByAnother];
}

- (void)showAlertWithError:(NSError *)error {
    [self showAlertWithTitle:error.localizedDescription message:error.localizedRecoverySuggestion ignoreIfBlockedByAnother:YES];
}

#pragma mark - Popup

- (void)showPopupWithTitle:(NSString *)title description:(NSString *)description buttonText:(NSString *)buttonText didCloseAction:(void (^ __nullable)(void))closeAction {
    LTPopupViewController *controller = [[LTPopupViewController alloc] initWithNibName:@"LTPopupViewController" bundle:nil];
    [controller setTitleText:title];
    [controller setDescriptionText:description];
    [controller setButtonText:buttonText];
    controller.modalPresentationStyle = UIModalPresentationCustom;
    controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    if (![self isShowingUI]) {
        [self willShowLootsieUI:nil];
        controller.closeAction = ^{
            if (closeAction != nil) {
                closeAction();
            }
            [self closeUI];
        };
    }
    else {
        controller.closeAction = closeAction;
    }
    UIViewController *topController = [self topViewControllerWithRootViewController:self.wrapRootViewController];
    [topController presentViewController:controller animated:YES completion:nil];
}

#pragma mark - Presentation
- (UIViewController *)presentationContext {
    UIViewController *context;
    if ([self.delegate respondsToSelector:@selector(presentationContextForUIManager)]) {
        context = [self.delegate presentationContextForUIManager];
    }
    
    if (!context) {
        context = [self topViewControllerWithRootViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
    }
    
    return context;
}

- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
	if ([rootViewController isKindOfClass:[UITabBarController class]]) {
		UITabBarController* tabBarController = (UITabBarController*)rootViewController;
		return [self topViewControllerWithRootViewController:tabBarController.selectedViewController];
	} else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
		UINavigationController* navigationController = (UINavigationController*)rootViewController;
		return [self topViewControllerWithRootViewController:navigationController.visibleViewController];
	} else if (rootViewController.presentedViewController && !rootViewController.presentedViewController.isBeingDismissed) {
		UIViewController* presentedViewController = rootViewController.presentedViewController;
		return [self topViewControllerWithRootViewController:presentedViewController];
	} else {
		return rootViewController;
	}
}

- (void)willShowLootsieUI:(void (^ __nullable)(void))completion {
    // grab presentation context
    UIViewController *presentationContext = [self presentationContext];
    
    // notify delegate of opening UI
    if ([self.delegate respondsToSelector:@selector(lootsieUIManagerWillShowUI)]) {
        [self.delegate lootsieUIManagerWillShowUI];
    }
    
    // present Lootsie's UI
    [presentationContext presentViewController:self.wrapRootViewController animated:NO completion:completion];
}

#pragma mark - Queue Delegate
/// Catch all Operation Queue errors
- (void)operationQueue:(LTOperationQueue *)queue operationDidFinish:(NSOperation *)operation withErrors:(NSArray *)errors {
    // Don't forget to call super
    [super operationQueue:queue operationDidFinish:operation withErrors:errors];
    //if (![queue isEqual:[LTNetworkManager lootsieManager].networkQueue]) return;
    /*
     We only want to show the first error, since subsequent errors might
     be caused by the first.
     */
    if (errors && errors.firstObject) {
        NSError *error = errors.firstObject;
        
        // Ignore operations that erred because conditions. This avoids infinite operations
        if ([error.domain isEqualToString:LTOperationErrorDomain] && error.code == LTOperationErrorCodeCondition) return;
        
        if (operation.isCancelled) {
            [self showAlertWithError:error];
        
            /// If the operation that fails is the init operation, we should close the UI
            if ([operation isKindOfClass:[LTInitSequenceOperation class]]) {
                [self closeUI];
            }
        }
    }
}

@end

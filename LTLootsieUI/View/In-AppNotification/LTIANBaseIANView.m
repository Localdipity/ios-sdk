//  LTIANBaseIANView.m
//  Created by Alexander Dovbnya on 3/25/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTIANBaseIANView.h"

@interface LTIANBaseIANView ()

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *detail;
@property (nonatomic, assign) NSInteger points;
@property (nonatomic, strong) NSString *currencyName;
@property (nonatomic, strong) UIColor *textColor;

@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *detailLabel;
@property (nonatomic, weak) IBOutlet UILabel *pointsLabel;
@property (nonatomic, weak) IBOutlet UILabel *currencyLabel;

// managing constraints for status bar
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *imageYConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bodyYConstraint;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *pointsYConstraint;

@end

@implementation LTIANBaseIANView

- (void)setupIANWithTitle:(NSString *)title detail:(NSString *)detail points:(NSInteger)points currencyName:(NSString *)currencyName textColor:(UIColor *)textColor {
    self.title = title;
    self.detail = detail;
    self.points = points;
    self.currencyName = (currencyName ?: NSLocalizedStringFromTable(@"POINTS", @"LTLootsie", @"Currency name to be displayed in case it's missing"));
    self.textColor = (textColor ?: [UIColor whiteColor]);
    [self updateView];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self updateView];
}

- (void)updateView {
    if (!self.titleLabel) {NSLog(@"THIS HAS BEEN CALLED"); return;} // IBOutlets not yet connected
    
    self.titleLabel.text = self.title;
    self.detailLabel.text = self.detail;
    self.pointsLabel.text = [NSString stringWithFormat:@"%ld", (long)self.points];
    self.currencyLabel.text = self.currencyName;
    
    self.titleLabel.textColor = self.textColor;
    self.detailLabel.textColor = self.textColor;
    self.pointsLabel.textColor = self.textColor;
    self.currencyLabel.textColor = self.textColor;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat oldPreferred = self.detailLabel.preferredMaxLayoutWidth;
    CGFloat newPreferred = CGRectGetWidth(self.detailLabel.superview.frame);
    if (oldPreferred != newPreferred) {
        self.detailLabel.preferredMaxLayoutWidth = newPreferred;
        [self setNeedsLayout];
        [super layoutSubviews];
    }
}

- (void)setTranslucenceHeight:(CGFloat)translucenceHeight {
    self.imageYConstraint.constant = translucenceHeight * .5f;
    self.bodyYConstraint.constant = translucenceHeight * .5f;
    self.pointsYConstraint.constant = translucenceHeight * .5f;
}

@end

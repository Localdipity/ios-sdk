//  LTAboutPageController.m
//  Created by Fabio Teles on 5/20/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
#import <CoreLocation/CoreLocation.h>

#import "LTAboutPageController.h"
#import "LTConstants.h"
#import "LTUIManager.h"
#import "LTNetworkManager.h"

#import "LTApp.h"
#import "LTData.h"
#import "LTLocationOperation.h"

static NSString * AboutCellIdentifier = @"AboutCell";

@interface LTAboutPageController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray *allInfo;
@property (nonatomic, strong) CLLocation *location;
@property (nonatomic, strong) LTOperationQueue *operationQueue;

@end

@implementation LTAboutPageController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.operationQueue = [LTOperationQueue new];
    self.operationQueue.name = [NSString stringWithFormat:@"%@Queue", NSStringFromClass([self class])];
    
    self.aboutTableView.allowsSelection = NO;
	[self populateData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)populateData {
    self.allInfo = @[@{@"name": NSLocalizedStringFromTable(@"Loading", @"LTLootsie", nil),
                       @"value": NSLocalizedStringFromTable(@"Please wait", @"LTLootsie", nil)}];
    
    __weak __typeof(self) weakSelf = self;
    [[LTUIManager sharedManager] startIfNeededWithSuccess:^{
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        strongSelf.allInfo = @[
                               @{
                                   @"name": NSLocalizedStringFromTable(@"Application", @"LTLootsie", nil),
                                   @"value" : ([LTUIManager sharedManager].data.app.name ?: @"(null)")
                                   },
                               @{
                                   @"name": NSLocalizedStringFromTable(@"Platform", @"LTLootsie", nil),
                                   @"value" : @"iOS"
                                   },
                               @{
                                   @"name": NSLocalizedStringFromTable(@"Device", @"LTLootsie", nil),
                                   @"value" : [UIDevice currentDevice].model
                                   },
                               @{
                                   @"name": NSLocalizedStringFromTable(@"Firmware", @"LTLootsie", nil),
                                   @"value" : [[UIDevice currentDevice] systemVersion]
                                   },
                               @{
                                   @"name": NSLocalizedStringFromTable(@"Language", @"LTLootsie", nil),
                                   @"value" : [[NSLocale currentLocale] objectForKey: NSLocaleLanguageCode]
                                   },
                               @{
                                   @"name": NSLocalizedStringFromTable(@"Country", @"LTLootsie", nil),
                                   @"value" : [[NSLocale currentLocale] objectForKey: NSLocaleCountryCode]
                                   },
                               [@{
                                   @"name": NSLocalizedStringFromTable(@"Location", @"LTLootsie", nil),
                                   @"value" : @"Fetching location"
                                   } mutableCopy],
                               @{
                                   @"name": NSLocalizedStringFromTable(@"IDFV", @"LTLootsie", nil),
                                   @"value" : [[[UIDevice  currentDevice] identifierForVendor] UUIDString]
                                   },
                               @{
                                   @"name": NSLocalizedStringFromTable(@"SDK Version", @"LTLootsie", nil),
                                   @"value" : LTSDKVersion
                                   },
                               @{
                                   @"name": NSLocalizedStringFromTable(@"API Server URL", @"LTLootsie", nil),
                                   @"value" : [[[LTNetworkManager lootsieManager] baseURL] absoluteString]
                                   }
                               ];
        
        [strongSelf.aboutTableView reloadData];
        [strongSelf fetchLocation];
    } failure:^(NSError *error) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        strongSelf.allInfo = @[];
        [strongSelf.aboutTableView reloadData];
    }];

    [self.aboutTableView reloadData];
}

- (void)fetchLocation {
    if (!self.location) {
        __weak __typeof(self) weakSelf = self;
        LTLocationOperation *locationOp = [[LTLocationOperation alloc] initWithAccuracy:kCLLocationAccuracyKilometer requestIfNotAvail:NO locationHandler:^(CLLocation *locationReceived) {
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            strongSelf.location = locationReceived;
            
            NSIndexPath *locationIndexPath = [NSIndexPath indexPathForRow:6 inSection:0];
            NSString *locationString = NSLocalizedStringFromTable(@"Not available", @"LTLootsie", nil);
            if (strongSelf.location) {
                locationString = [NSString stringWithFormat:@"%f, %f", strongSelf.location.coordinate.latitude, strongSelf.location.coordinate.longitude];
            }
            strongSelf.allInfo[locationIndexPath.row][@"value"] = locationString;
            [strongSelf.aboutTableView reloadRowsAtIndexPaths:@[locationIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }];
        [self.operationQueue addOperation:locationOp];
    }
}

#pragma mark - UITableView Delegate and DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return self.allInfo.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:AboutCellIdentifier forIndexPath:indexPath];

	NSDictionary *info = [self.allInfo objectAtIndex:indexPath.row];

	cell.textLabel.text = [info objectForKey:@"name"];
	cell.detailTextLabel.text = [info objectForKey:@"value"];

	return cell;
}

- (BOOL)tableView:(UITableView *)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.allInfo.count > 1;
}

-(void)tableView:(UITableView*)tableView performAction:(SEL)action forRowAtIndexPath:(NSIndexPath*)indexPath withSender:(id)sender {
    UIPasteboard* pasteboard = [UIPasteboard generalPasteboard];
    NSDictionary *info = [self.allInfo objectAtIndex:indexPath.row];
    pasteboard.string = [info objectForKey:@"value"];
}

-(BOOL)tableView:(UITableView*)tableView canPerformAction:(SEL)action forRowAtIndexPath:(NSIndexPath*)indexPath withSender:(id)sender {
    if (action == @selector(copy:)) {
        return YES;
    }
    return NO;
}

@end

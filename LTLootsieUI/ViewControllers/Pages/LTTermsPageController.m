//  LTTermsPageController.m
//  Created by Fabio Teles on 5/20/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTTermsPageController.h"
#import "LTTermsCell.h"
#import "LTTools.h"

static NSString * TermCellIdentifier = @"TermCell";

@interface LTTermsPageController () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSArray *terms;
@property (weak, nonatomic) IBOutlet UITableView *termsTableView;

@end

@implementation LTTermsPageController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

	self.termsTableView.rowHeight = UITableViewAutomaticDimension;
	self.termsTableView.estimatedRowHeight = 110.f;

	[self populateData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)populateData {
	self.terms = @[
				   @{
					   @"title": @"Fraud",
					   @"blurb" : @"Lootsie reserves the right to refuse, cancel or hold for review gift cards and orders, redemptions or claims for suspected fraud, for cards mistakenly issued in an incorrect denomination, or for other violations of gift card policies."
					   },
				   @{
					   @"title": @"Returns",
					   @"blurb" : @"All returns and/or exchanges or customer service issues must be dealt with through that card's retailer. Lootsie, Inc. is not responsible for any of the above mentioned. Once a gift card or special offer has been redeemed through Lootsie, Inc., any and all further engagement with the gift card/offer provider must be through that retailer."
					   },
				   @{
					   @"title": @"Resale",
					   @"blurb" : @"Gift cards/discounts cannot be resold or combined with any other offer, unless specifically stated."
					   },
				   @{
					   @"title": @"Redemption",
					   @"blurb" : @"If the order total is more than the gift card amount, the remaining balance must be paid for by cash, credit/debit card, PayPal account, product voucher or additional gift card(s). The gift card balance will immediately be reduced. Upon redemption, any unused balance on the gift card will be carried over for future purchases. Balances cannot be redeemed or exchanged for cash."
					   },
				   @{
					   @"title": @"Expiration",
					   @"blurb" : @"Gift cards and special offers may have an expiration date listed on Lootsie.com and will not be valid after that date. "
					   },
				   @{
					   @"title": @"Refunds",
					   @"blurb" : @"Gift cards may not be returned or cancelled after redemption. There are no refunds of Lootsie Points."
					   },
				   @{
					   @"title": @"Lost or stolen gift cards",
					   @"blurb" : @"Gift cards will not be replaced if lost or stolen. Treat them like gold."
					   }
				   ];
}

- (void)setupCell:(LTTermsCell *)cell withTerm:(NSDictionary *)dictionary {
	cell.blurbLabel.text = dictionary[@"blurb"];
}

#pragma mark - UITableView Delegate and DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return self.terms.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	NSDictionary *terms = self.terms[section];

	UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, 320.f, 48.f)];
	header.userInteractionEnabled = NO;
	header.backgroundColor = [LTTools colorWithHex:0xf0f0f0];

	UILabel *title = [UILabel new];
	UIFont *font;
	if ([UIFont respondsToSelector:@selector(systemFontOfSize:weight:)]) {
		font = [UIFont systemFontOfSize:16.f weight:UIFontWeightMedium];
	} else {
		font = [UIFont boldSystemFontOfSize:16.f];
	}
	title.font = font;
	title.textColor = [UIColor blackColor];
	title.text = terms[@"title"];

	title.translatesAutoresizingMaskIntoConstraints = NO;
	[header addSubview:title];

	[header addConstraint:[NSLayoutConstraint constraintWithItem:title attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:header attribute:NSLayoutAttributeCenterX multiplier:1.f constant:0.f]];
	[header addConstraint:[NSLayoutConstraint constraintWithItem:title attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:header attribute:NSLayoutAttributeCenterY multiplier:1.f constant:0.f]];
	header.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);

	return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return 40.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	LTTermsCell *cell = [tableView dequeueReusableCellWithIdentifier:TermCellIdentifier forIndexPath:indexPath];
	[self setupCell:cell withTerm:self.terms[indexPath.section]];

	[cell setNeedsLayout];
	[cell layoutIfNeeded];

	return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	static LTTermsCell *sizingCell;

	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sizingCell = [tableView dequeueReusableCellWithIdentifier:TermCellIdentifier];
	});

	sizingCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.frame), CGRectGetHeight(sizingCell.bounds));

	[self setupCell:sizingCell withTerm:self.terms[indexPath.section]];

	[sizingCell setNeedsLayout];
	[sizingCell layoutIfNeeded];

	CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
	return roundf(size.height + 1.f); // account for separator
}

@end

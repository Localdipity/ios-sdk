//  LTVideoViewController.m
//  Created by Alexander Dovbnya on 2/16/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTVideoViewController.h"
#import "LTVideoView.h"

#import "LTUIManager.h"

@interface LTVideoViewController ()

@property (weak, nonatomic) IBOutlet LTVideoView *videoView;

@end

@implementation LTVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.videoView.player = [AVPlayer playerWithURL:self.videoUrl];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.videoView play];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerDidEndPlay:) name:AVPlayerItemDidPlayToEndTimeNotification object:self.videoView.player.currentItem];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)hideButtonDidPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark Nottification

- (void)applicationDidBecomeActive:(NSNotification *)nottifcation {
    [self.videoView play];
}

- (void)playerDidEndPlay:(NSNotification *)nottifcation {
    [self dismissViewControllerAnimated:YES completion:^{
        [[LTUIManager sharedManager] showPopupWithTitle:NSLocalizedStringFromTable(@"Congrats!", @"LTLootsie", nil)
                                            description:NSLocalizedStringFromTable(@"You just earned Lootsie Points for watching a video", @"LTLootsie", nil)
                                             buttonText:NSLocalizedStringFromTable(@"Ok", @"LTLootsie", nil)
                                         didCloseAction:nil];
    }];

}

@end

//  LTPopupViewController.m
//  Created by Alexander Dovbnya on 2/17/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "LTPopupViewController.h"

@interface LTPopupViewController () <UIViewControllerTransitioningDelegate>

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UIView *container;

@end

@implementation LTPopupViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    
    self.titleLabel.text = self.titleText;
    self.descriptionLabel.text = self.descriptionText;
    [self.button setTitle:[self.buttonText uppercaseString] forState:UIControlStateNormal];
    
    self.button.layer.cornerRadius = 5.f;
    
    self.container.layer.shadowColor = [UIColor blackColor].CGColor;
    self.container.layer.shadowOpacity = 0.3f;
    self.container.layer.shadowOffset = CGSizeZero;
    self.container.layer.shadowRadius = 8.f;
    self.container.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.container.bounds].CGPath;
    self.container.layer.cornerRadius = 7.f;
}

- (IBAction)didPressButton:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:self.closeAction];
}

@end

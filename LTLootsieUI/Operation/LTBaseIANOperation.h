//  LTBaseIANOperation.h
//  Created by Fabio Teles on 3/25/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <Foundation/Foundation.h>
#import "LTOperation.h"

@class LTIANBaseIANView;

@interface LTBaseIANOperation : LTOperation

@property (nonatomic, strong) LTIANBaseIANView *ianView;

@property (nonatomic, assign) NSTimeInterval duration;
@property (nonatomic, readonly, strong) UIColor *textColor;
@property (nonatomic, readonly, strong) UIColor *backgroundColor;
@property (nonatomic, readonly, strong) NSString *currencyName;

- (instancetype)initWithCurrencyName:(NSString *)currencyName
                          textColor:(UIColor *)textColor
                    backgroundColor:(UIColor *)backgroundColor
                             action:(void (^)(void))action
           presentationContextBlock:(UIViewController* (^)(void))presentationContextBlock;

- (LTIANBaseIANView *)createIANView;
- (CGFloat)heightIANView;
- (void)closeDidPressed:(id)sender;
- (void)handleIANTap:(id)sender;

@end

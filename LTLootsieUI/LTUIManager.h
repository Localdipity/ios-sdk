//  LTUIManager.h
//  Created by Fabio Teles on 5/19/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTLootsie.h"
#import "LTMenuController.h"

#define LT_SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

typedef NS_ENUM(NSUInteger, LTRenderingMode) {
    LTRenderingModeAutomatic, // default value
    LTRenderingModeLandscape,
    LTRenderingModePortrait,
    LTRenderingModePortraitAsLandscape,
    LTRenderingModeLandscapeAsPortrait
};

typedef NS_ENUM(NSUInteger, LTIANBehavior) {
    LTIANBehaviorGoToRewards, // default value
    LTIANBehaviorGoToAchievements,
    LTIANBehaviorGoToAbout,
    LTIANBehaviorGoToTOS,
    LTIANBehaviorDoNothing,
    LTIANBehaviorDoNotAppear
};

extern  NSString * const _Nonnull  LTDismissedAlertKey;

@class LTUIManager;
/**
 LTUIManagerDelegate is a protocol that can be implemented to be notified when
 important UI changes occur to the Lootsie'UI. For example, you can optionally be
 notified when when the UI is shown or dismissed so you can pause or resume your
 game
 */
@protocol LTUIManagerDelegate <NSObject>
@optional
- (void)lootsieUIManagerWillShowUI;
- (void)lootsieUIManagerDidCloseUI;
- (nullable UIViewController *)presentationContextForUIManager;

@end

@class LTAchievement, LTAlertOperation;
/*!
 * @class LTUIManager
 * Main class that serves for Lootsie UI display
 */
@interface LTUIManager : LTManager

/*!
 *  Read-only property to access the menu view controller
 */
@property (nonatomic, readonly, strong, nonnull) LTMenuController *menuController;

/*!
 *   Rendering mode property that allows you to force Portrait or Landscape modes.
 *   Defaults to LTRenderingModeAutomatic. See LTRenderingMode for the list of possible
 *   values.
 */
@property (nonatomic, assign) LTRenderingMode renderingMode;

/*!
 *   Defines the behavior of the IAN (In-App Notification). Defaults to LTIANBehaviorGoToRewards.
 *   See LTIANBehavior for the list of possible values.
 */
@property (nonatomic, assign) LTIANBehavior ianBehavior;

/*!
 *  Toggles between showing Alerts only when the UI is open or not. Defaults to YES.
 */
@property (nonatomic, assign) BOOL onlyShowAlertsWhenUIOpen;

/*!
 *   A delegate object that's notified when important UI changes occur.
 *   @see LTUIManagerDelegate
 */
@property (nonatomic, weak) id<LTUIManagerDelegate> delegate;

/*!
 *   Shows the Terms of Service page of Lootsie's UI by opening it if not being displayed
 *   or by switching to this page is already being displayed
 */
- (void)showTermsPage;

/*!
 *   Shows the About page of Lootsie's UI by opening it if not being displayed or by
 *   switching to this page is already being displayed
 */
- (void)showAboutPage;


/*!
 *  @return YES if Lootsie's UI is currently being displayed
 */
- (BOOL)isShowingUI;

/*!
 *   Closes Lootsies's UI if it's currently begin displayed, otherwise it does not perform
 *   any action
 */
- (void)closeUI;

/*!
 *   Show an alert dialog with a customized LTAlertOperation
 *
 *   @see LTAlertOperation
 *
 *  @param alert Operation with alert data
 */
- (void)showAlert:(nonnull LTAlertOperation *)alert;

/*!
 *   A handy way to create a dialog box with only an "Ok" action button without having
 *   to build a LTAlertOperation by yourself
 *
 *  @param title   title alert
 *  @param message message alert
 */
- (void)showAlertWithTitle:(nonnull NSString *)title message:(nonnull NSString *)message;

/*!
 *   Easy way to show an alert dialog by providing a NSError object
 *
 */
- (void)showAlertWithError:(nonnull NSError *)error;

/*!
 *   Show popup view
 *
 *  @param title       title popup
 *  @param description description popup
 *  @param buttonText  text of button
 *  @param closeAction action when user press button in popup
 */
- (void)showPopupWithTitle:(nonnull NSString *)title description:(nonnull NSString *)description buttonText:(nonnull NSString *)buttonText didCloseAction:(void (^ __nullable)(void))closeAction;

@end

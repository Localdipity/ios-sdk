//  LTLogoutUserSequenceOperation.m
//  Created by Fabio Teles on 9/29/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTLogoutUserSequenceOperation.h"

#import "LTUserSessionOperation.h"
#import "LTBlockOperation.h"

#import "LTManager.h"
#import "LTData.h"

@implementation LTLogoutUserSequenceOperation

+ (instancetype)sequence {
    // Logout
    LTUserSessionOperation *logoutOp = [LTUserSessionOperation DELETE];
    
    // Grab guest user session so we don't leave the SDK in 'Ghost mode'
    LTUserSessionOperation *guestOp = [LTUserSessionOperation POSTAsGuest];
    
    // Update
    LTData *data = [LTManager sharedManager].data;
    LTBlockOperation *updateDataOp = [LTBlockOperation autoSuccessOpWithBlock:^{
        data.user = nil;
        data.userSessionToken = guestOp.userSessionToken;
    }];
    
    [guestOp addDependency:logoutOp];
    [updateDataOp addDependency:guestOp];
    
    return [self groupWithOperations:@[logoutOp, guestOp, updateDataOp]];
}

@end

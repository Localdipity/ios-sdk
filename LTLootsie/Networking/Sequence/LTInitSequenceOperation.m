//  LTInitSequenceOperation.m
//  Created by Fabio Teles on 7/31/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTInitSequenceOperation.h"

#import "LTApiSessionOperation.h"
#import "LTUserSessionOperation.h"
#import "LTAppOperation.h"
#import "LTSettingsOperation.h"
#import "LTLocationOperation.h"

#import "LTApp.h"
#import "LTSettings.h"

#import "LTManager.h"
#import "LTData.h"

@interface LTInitSequenceOperation ()

@end

@implementation LTInitSequenceOperation

+ (instancetype)sequenceAsGuestWithAppSecret:(NSString *)appSecret
                                sendLocation:(BOOL)sendLocation {
    return [self sequenceWithAppSecret:appSecret emailIfNotLogged:nil sendLocation:sendLocation];
}

+ (instancetype)sequenceWithAppSecret:(NSString *)appSecret
                     emailIfNotLogged:(NSString *)email
                         sendLocation:(BOOL)sendLocation {
    
    NSParameterAssert(appSecret);
    
    // Check if app secret matches
    LTData *data = [LTManager sharedManager].data;
    
    if (![data.appSecret isEqual:appSecret]) {
        if (data.appSecret) { // only if set before we need to clear the old sessions
            data.apiSessionToken = nil;
            data.userSessionToken = nil;
        }
        data.appSecret = appSecret;
    }
    
    NSMutableArray *operations = [NSMutableArray array];
    NSMutableArray *trivialOperations = [NSMutableArray array];
    
        // API SESSION OPERATION
        if (!data.apiSessionToken) {
        __block CLLocation *location;
        LTApiSessionOperation *apiSessionOp = [LTApiSessionOperation POSTWithAppSecret:appSecret locationBlock:^{return location;}];
        
        if (sendLocation) {
            LTLocationOperation *locationOp = [[LTLocationOperation alloc] initWithAccuracy:kCLLocationAccuracyKilometer
                                                                            locationHandler:^(CLLocation *receivedLocation) {
                                                                                location = receivedLocation;
                                                                            }];
            
            [apiSessionOp addDependency:locationOp];
            [trivialOperations addObject:locationOp];
        }
        
        [operations addObject:apiSessionOp];
    }
    
        // USER SESSION OPERATION
        if (!data.userSessionToken) {
        LTUserSessionOperation *guestUserSessionOp = [LTUserSessionOperation POSTAsGuest];
        if (operations.lastObject) [guestUserSessionOp addDependency:operations.lastObject];
        
        [operations addObject:guestUserSessionOp];
        
        /** if an e-mail was provided to log in existent user, we do that right
         after loggin in as a guest and adjust dependencies accordingly
         */
        
        if (email) {
            LTUserSessionOperation *emailUserSessionOp = [LTUserSessionOperation POSTWithEmailOnly:email];
            [emailUserSessionOp addDependency:guestUserSessionOp];
            [operations addObject:emailUserSessionOp];
        }
    }
    
        // APP & SETTINGS OPERATIONS
        LTAppOperation *appOperation = [LTAppOperation GET];
    LTSettingsOperation *settingsOperation = [LTSettingsOperation GET];
    
    if (operations.lastObject) {
        [appOperation addDependency:operations.lastObject];
        [settingsOperation addDependency:operations.lastObject];
    }
    [operations addObjectsFromArray:@[appOperation, settingsOperation]];
    
    return [self groupWithOperations:operations trivialOperations:trivialOperations];
}

- (void)operationDidFinish:(NSOperation *)operation withErrors:(NSArray *)errors {
    [super operationDidFinish:operation withErrors:errors];
    if (!self.isCancelled) {
        LTData *data = [LTManager sharedManager].data;
        
        if ([operation isKindOfClass:[LTApiSessionOperation class]]) {
            data.apiSessionToken = ((LTApiSessionOperation *)operation).apiSessionToken;
        } else if ([operation isKindOfClass:[LTUserSessionOperation class]]) {
            LTUserSessionOperation *userOperation = (LTUserSessionOperation *)operation;
            if (userOperation.userSessionToken != nil) {
               data.userSessionToken = userOperation.userSessionToken;
            }
        } else if ([operation isKindOfClass:[LTAppOperation class]]) {
            data.app = ((LTAppOperation *)operation).app;
        } else if ([operation isKindOfClass:[LTSettingsOperation class]]) {
            data.settings = ((LTSettingsOperation *)operation).settings;
        }
    }
}

@end

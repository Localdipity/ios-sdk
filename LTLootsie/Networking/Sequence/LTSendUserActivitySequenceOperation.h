//  LTSendUserActivitySequenceOperation.h
//  Created by Fabio Teles on 9/16/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTSequence.h"

@class LTUserActivity;
@interface LTSendUserActivitySequenceOperation : LTSequence

/**
 Informs the API about an user activity that occured.
 
 @param metric The metric used for the activity
 @param value The value to be recorded for this activity
 */
+ (instancetype)sequenceWithMetric:(NSString *)metric
                             value:(NSString *)value;

/**
 Informs the API about an user activity that occured.
 
 @param metric The metric used for the activity
 @param value The value to be recorded for this activity
 @param activityId An optional activityId to help you identify this activity
 */
+ (instancetype)sequenceWithMetric:(NSString *)metric
                             value:(NSString *)value
                        activityId:(NSString *)activityId;

/**
 Informs the API about an user activity that occured.
 
 @param userActivity The userActivity object to be used
 */
+ (instancetype)sequenceWithUserActivity:(LTUserActivity *)userActivity;

@end

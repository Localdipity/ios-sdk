//  LTUserSessionGuestOperation.m
//  Created by Fabio Teles on 7/27/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTUserSessionOperation.h"
#import "LTNetworkManager.h"
#import "LTModelEntity.h"

#import "LTAFURLResponseSerialization.h"

@interface LTUserSessionOperation ()

@property (nonatomic, readwrite, strong) NSString *userSessionToken;
@property (nonatomic, assign) BOOL merging;

@end

@implementation LTUserSessionOperation

+ (instancetype)POSTAsGuest {
    LTRequestBuilder requestBuilder = ^NSURLRequest *(NSError *__autoreleasing *error) {
        
        NSDictionary *additionalHeaders = [self authHeadersIncludingAppSecret:YES apiSession:YES userSession:NO];
        
        return [[LTNetworkManager lootsieManager] requestWithHTTPMethod:@"POST"
                                                                   path:LTAPIUserSessionGuestPath
                                                             parameters:nil
                                                  additionalHTTPHeaders:additionalHeaders
                                                                  error:error];
    };
    
    
    return [[self alloc] initWithRequestBuilder:requestBuilder];
}

+ (instancetype)POSTWithEmailOnly:(NSString *)email {
    NSParameterAssert(email);
    
    LTRequestBuilder requestBuilder = ^NSURLRequest *(NSError *__autoreleasing *error) {
        
        // User session is required here. In order to logged in an user, it must be a guest user beforehand.
        NSDictionary *additionalHeaders = [self authHeadersIncludingAppSecret:YES apiSession:YES userSession:YES];
        
        NSDictionary *parameters = @{LTEmailKey: email};
        
        return [[LTNetworkManager lootsieManager] requestWithHTTPMethod:@"POST"
                                                                   path:LTAPIUserSessionEmailOnlyPath
                                                             parameters:parameters
                                                  additionalHTTPHeaders:additionalHeaders
                                                                  error:error];
    };
    
    LTUserSessionOperation *op = [[self alloc] initWithRequestBuilder:requestBuilder];
    op.merging = YES;
    return op;
}

+ (instancetype)DELETE {
    LTRequestBuilder requestBuilder = ^NSURLRequest *(NSError *__autoreleasing *error) {
        
        NSDictionary *additionalHeaders = [self authHeadersIncludingAppSecret:YES apiSession:NO userSession:YES];
        
        return [[LTNetworkManager lootsieManager] requestWithHTTPMethod:@"DELETE"
                                                                   path:LTAPIUserSessionPath
                                                             parameters:nil
                                                  additionalHTTPHeaders:additionalHeaders
                                                                  error:error];
    };
    
    return [[self alloc] initWithRequestBuilder:requestBuilder];
}

- (NSError *)dataTaskDidFinishSuccessfully {
    if ([[self HTTPMethod] isEqualToString:@"POST"]) {
        self.userSessionToken = [(NSDictionary *)self.responseObject objectForKeyNilSafe:LTHeaderUserSessionTokenKey];
    }
    return nil;
}

- (NSError *)formatDataTaskError:(NSError *)error {
    NSError *baseError = [super formatDataTaskError:error];
    
    NSMutableDictionary *mutableInfo;
    NSHTTPURLResponse *response = [baseError.userInfo objectForKeyNilSafe:LTAFNetworkingOperationFailingURLResponseErrorKey];

    if (response && [[self HTTPMethod] isEqualToString:@"POST"]) {
        if (response.statusCode == 409) {
            NSDictionary *responseErrorDic = [[baseError.userInfo objectForKeyNilSafe:LTNetworkResponseErrorsJSONKey] firstObject];
            if (responseErrorDic && [responseErrorDic[LTErrorMessageKey] rangeOfString:@"LP"].location != NSNotFound) {
                mutableInfo = [baseError.userInfo mutableCopy];
                mutableInfo[NSLocalizedDescriptionKey] = NSLocalizedStringFromTable(@"You're too good!", @"LTLootsie", nil);
                mutableInfo[NSLocalizedRecoverySuggestionErrorKey] = NSLocalizedStringFromTable(@"Looks like you've hit the daily app max. Dont worry, you'll keep earning more tomorrow!", @"LTLootsie", nil);
            } else {
                mutableInfo = [baseError.userInfo mutableCopy];
                mutableInfo[NSLocalizedDescriptionKey] = NSLocalizedStringFromTable(@"Uh oh!", @"LTLootsie", nil);
                mutableInfo[NSLocalizedRecoverySuggestionErrorKey] = NSLocalizedStringFromTable(@"E-mail address already registered", @"LTLootsie", nil);
            }
        } else if (self.merging) {
            mutableInfo = [baseError.userInfo mutableCopy];
            mutableInfo[NSLocalizedDescriptionKey] = NSLocalizedStringFromTable(@"Uh oh!", @"LTLootsie", nil);
            mutableInfo[NSLocalizedRecoverySuggestionErrorKey] = NSLocalizedStringFromTable(@"Sorry, but points cannot be merged at this time.", @"LTLootsie", nil);
            mutableInfo[NSUnderlyingErrorKey] = error;
        }
    }
    
    return (mutableInfo ? [NSError errorWithDomain:baseError.domain code:baseError.code userInfo:mutableInfo] : baseError);
}

@end

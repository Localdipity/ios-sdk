//  LTApiSessionOperation.m
//  Created by Fabio Teles on 7/14/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTApiSessionOperation.h"
#import "LTNetworkManager.h"
#import "LTModelEntity.h"
#import <CoreLocation/CoreLocation.h>

@interface LTApiSessionOperation ()

@property (nonatomic, readwrite, strong) NSString *apiSessionToken;

@end

@implementation LTApiSessionOperation

+ (instancetype)POSTWithAppSecret:(NSString *)appSecret
                    locationBlock:(CLLocation * (^)(void))locationBlock {
    NSLocale *locale = [NSLocale currentLocale];
    return [self POSTWithAppSecret:appSecret
                        apiVersion:LTAPIVersion
                          platform:LTPlatform
                            device:[UIDevice currentDevice].model
                          firmware:[[UIDevice currentDevice] systemVersion]
                      languageCode:[locale objectForKey: NSLocaleLanguageCode]
                       countryCode:[locale objectForKey: NSLocaleCountryCode]
                     locationBlock:locationBlock];
}

+ (instancetype)POSTWithAppSecret:(NSString *)appSecret
                       apiVersion:(NSString *)apiVersion
                         platform:(NSString *)platform
                           device:(NSString *)device
                         firmware:(NSString *)firmware
                     languageCode:(NSString *)languageCode
                      countryCode:(NSString *)countryCode
                    locationBlock:(CLLocation * (^)(void))locationBlock {
    
    NSParameterAssert(appSecret);
    NSParameterAssert(apiVersion);
    NSParameterAssert(platform);
    NSParameterAssert(device);
    NSParameterAssert(firmware);

    LTRequestBuilder requestBuilder = ^NSURLRequest *(NSError *__autoreleasing *error) {
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           apiVersion,  LTAPIVersionKey,
                                           platform,    LTPlatformKey,
                                           device,      LTDeviceKey,
                                           firmware,    LTFirmwareKey,
                                           nil];
        
        if (locationBlock) {
            CLLocation *location = locationBlock();
            if (location) {
                NSString *locationString = [NSString stringWithFormat:@"%f, %f", location.coordinate.latitude, location.coordinate.longitude];
                parameters[LTLatitudeLongitudeKey] = locationString;
            }
        }
        
        if (languageCode) {
            parameters[LTLanguageKey] = languageCode;
        }
        if (countryCode) {
            parameters[LTCountryKey] = countryCode;
        }
        
        NSDictionary *additionalHeaders = @{LTHeaderAppSecretKey: appSecret};
        
        return [[LTNetworkManager lootsieManager] requestWithHTTPMethod:@"POST"
                                                                   path:LTAPISessionPath
                                                             parameters:parameters
                                                  additionalHTTPHeaders:additionalHeaders
                                                                  error:error];
    };
    
    return [[self alloc] initWithRequestBuilder:requestBuilder];
}

- (NSError *)dataTaskDidFinishSuccessfully {
    if (self.responseObject) {
        self.apiSessionToken = [(NSDictionary *)self.responseObject objectForKeyNilSafe:LTHeaderAPISessionTokenKey];
    }
    return nil;
}

@end

//  LTSettingsOperation.m
//  Created by Fabio Teles on 7/23/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTSettingsOperation.h"
#import "LTNetworkManager.h"
#import "LTSettings.h"

NSInteger const LTSettingsErrorSDKNotEnabledCode       = -102;

@interface LTSettingsOperation ()

@property (nonatomic, readwrite, strong) LTSettings *settings;

@end

@implementation LTSettingsOperation

+ (instancetype)GET {
    NSLocale *locale = [NSLocale currentLocale];
    return [self GETWithAPIVersion:LTAPIVersion
                          platform:LTPlatform
                            device:[UIDevice currentDevice].model
                          firmware:[[UIDevice currentDevice] systemVersion]
                      languageCode:[locale objectForKey: NSLocaleLanguageCode]
                       countryCode:[locale objectForKey: NSLocaleCountryCode]];
}

+ (instancetype)GETWithAPIVersion:(NSString *)apiVersion
                         platform:(NSString *)platform
                           device:(NSString *)device
                         firmware:(NSString *)firmware
                     languageCode:(NSString *)languageCode
                      countryCode:(NSString *)countryCode {
    NSParameterAssert(apiVersion);
    NSParameterAssert(platform);
    NSParameterAssert(device);
    NSParameterAssert(firmware);
    
    LTRequestBuilder requestBuilder = ^NSURLRequest *(NSError *__autoreleasing *error) {
        
        NSMutableDictionary *parameters = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                           apiVersion,  LTAPIVersionKey,
                                           platform,    LTPlatformKey,
                                           device,      LTDeviceKey,
                                           firmware,    LTFirmwareKey,
                                           nil];
        
        if (languageCode) {
            parameters[LTLanguageKey] = languageCode;
        }
        if (countryCode) {
            parameters[LTCountryKey] = countryCode;
        }
        
        NSDictionary *additionalHeaders = [self authHeadersIncludingAppSecret:YES apiSession:NO userSession:YES];
        
        return [[LTNetworkManager lootsieManager] requestWithHTTPMethod:@"GET"
                                                                   path:LTAPISettingsPath
                                                             parameters:parameters
                                                  additionalHTTPHeaders:additionalHeaders
                                                                  error:error];
    };
    
    return [[self alloc] initWithRequestBuilder:requestBuilder];
}

- (NSError *)dataTaskDidFinishSuccessfully {
    self.settings = [LTSettings entityWithDictionary:self.responseObject];
    
    /// Check if SDK is disabled on the server (API) and return an error if it's
    
    if (!self.settings.isSdkEnabled) {
        NSDictionary *userInfo = @{
                                   NSLocalizedDescriptionKey: NSLocalizedStringFromTable(@"SDK is not enabled", @"LTLootsie", nil),
                                   NSLocalizedRecoverySuggestionErrorKey: NSLocalizedStringFromTable(@"Your application is not enabled on our servers.", @"LTLootsie", nil)
                                   };
        return [NSError errorWithDomain:LTNetworkErrorDomain code:LTSettingsErrorSDKNotEnabledCode userInfo:userInfo];
    }
    
    return  nil;
}


@end

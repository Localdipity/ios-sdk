//  LTUserSessionGuestOperation.h
//  Created by Fabio Teles on 7/27/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTDataTaskOperation.h"
@interface LTUserSessionOperation : LTDataTaskOperation

@property (nonatomic, readonly, strong) NSString *userSessionToken;

/**
 Retrieves an user session token from the API for a guests user. You then can make
 other API calls using that session token.
 
 @param completionHandler Block to be called when the API HTTP call returns, containning
 the user's session token or an error if the operation failed.
 */
+ (instancetype)POSTAsGuest;

/**
 Retrieves an user session token of the existing user associated with the e-mail
 provided, OR of a brand new user if the e-mail was never used before.
 
 @param completionHandler Block to be called when the API HTTP call returns. It'll 
 contain an error if the operation failed or nothing if it was successful.
 
 @warning You must have an user session token of a guest user when you call this 
 method. This method won't log in users with e-mail if a guest user is not present.
 */
+ (instancetype)POSTWithEmailOnly:(NSString *)email;

/**
 Deletes an user session, i.e.:, perform a logout for the given user session
 
 @param completionHandler Block to be called when the API HTTP call returns. It'll
 contain an error if the operation failed or nothing if it was successful.
 */
+ (instancetype)DELETE;

@end

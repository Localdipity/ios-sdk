//  LTLocationCondition.m
//  Created by Fabio Teles on 8/4/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <CoreLocation/CoreLocation.h>
#import "LTOperation.h"
#import "LTLocationCondition.h"
#import "LTMutuallyExclusiveCondition.h"

@interface LTLocationPermissionOperation : LTOperation <CLLocationManagerDelegate>

@property (nonatomic, assign) LTLocationUsage usage;
@property (nonatomic, strong) CLLocationManager *manager;

- (instancetype)initWithUsage:(LTLocationUsage)usage;

@end


@implementation LTLocationPermissionOperation

- (instancetype)initWithUsage:(LTLocationUsage)usage {
    if (self = [super init]) {
        _usage = usage;
        [self addCondition:[LTMutuallyExclusiveCondition alertMutuallyExclusive]];
    }
    return self;
}

- (void)execute {
    CLAuthorizationStatus actual = [CLLocationManager authorizationStatus];
    
    BOOL needsUpdate = NO;
    if ([CLLocationManager instancesRespondToSelector:@selector(requestWhenInUseAuthorization)]) {
        needsUpdate = (actual == kCLAuthorizationStatusAuthorizedWhenInUse && self.usage == LTLocationUsageAlways);
    }
    
    if (actual == kCLAuthorizationStatusNotDetermined || needsUpdate) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self requestPermission];
        });
    } else {
        [self finish:nil];
    }
}

- (void)requestPermission {
    self.manager = [CLLocationManager new];
    self.manager.delegate = self;
    
    if ([self.manager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        NSString *key;
        switch (self.usage) {
            case LTLocationUsageWhenInUse:
                key = @"NSLocationWhenInUseUsageDescription";
                [self.manager requestWhenInUseAuthorization];
                break;
            case LTLocationUsageAlways:
                key = @"NSLocationAlwaysUsageDescription";
                [self.manager requestAlwaysAuthorization];
        }
        
        // This is helpful while developing the app
        NSString __unused *reason = [NSString stringWithFormat:@"Requesting location permission requires the %@ key in your Info.plist", key];
        NSAssert([[NSBundle mainBundle] objectForInfoDictionaryKey:key] != nil, reason);
    } else {
        // iOS 7 support
        [self.manager startUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    if ([manager isEqual:self.manager] && self.isExecuting && status != kCLAuthorizationStatusNotDetermined) {
        // iOS 7: We need to stop updating location
        if (![manager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [manager stopUpdatingLocation];
        }
        [self finish:nil];
    }
}

@end

NSString * const LTLocationServicesEnabledKey       = @"CLLocationServicesEnabled";
NSString * const LTAuthorizationStatusKey           = @"CLAuthorizationStatus";

@implementation LTLocationCondition

- (instancetype)initWithUsage:(LTLocationUsage)usage {
    if ([super init]) {
        _usage = usage;
    }
    return self;
}

- (instancetype)init {
    return [self initWithUsage:LTLocationUsageWhenInUse];
}

- (NSString *)name {
    return NSStringFromClass([self class]);
}

- (BOOL)isMutuallyExclusive {
    return NO;
}

- (NSOperation *)dependencyForOperation:(LTOperation *)operation {
    return [[LTLocationPermissionOperation alloc] initWithUsage:self.usage];
}

- (void)evaluateForOperation:(LTOperation *)operation completion:(void (^)(LTOperationConditionResult, NSError *))completion {
    BOOL enabled = [CLLocationManager locationServicesEnabled];
    CLAuthorizationStatus actual = [CLLocationManager authorizationStatus];
    BOOL passed = NO;
    
    BOOL hasAlways = NO;
    // if enabled and has always kind of permission (also supporting iOS 7)
#if (defined(__IPHONE_OS_VERSION_MIN_REQUIRED) && __IPHONE_OS_VERSION_MIN_REQUIRED < 80000)
    hasAlways = actual == kCLAuthorizationStatusAuthorized;
#endif
    if (!hasAlways && [CLLocationManager instancesRespondToSelector:@selector(requestWhenInUseAuthorization)]) {
        hasAlways = actual == kCLAuthorizationStatusAuthorizedAlways;
    }
    if (enabled && hasAlways) {
        passed = YES;
    }
    
    // if enabled and has what we need in terms of authorization (iOS 8 and up only)
    if ([CLLocationManager instancesRespondToSelector:@selector(requestWhenInUseAuthorization)] &&
        enabled && actual == kCLAuthorizationStatusAuthorizedWhenInUse && self.usage == LTLocationUsageWhenInUse) {
        passed = YES;
    }
    
    /*
     Anything else is an error. Maybe location services are disabled,
     or maybe we need "Always" permission but only have "WhenInUse",
     or maybe access has been restricted or denied,
     or maybe access hasn't been request yet.
     
     The last case would happen if this condition were wrapped in a `SilentCondition`.
     */

    if (!passed) {
        NSError *error = [NSError conditionErrorWithUserInfo:@{
                                                               LTOperationConditionKey: self.name,
                                                               LTLocationServicesEnabledKey: @(enabled),
                                                               LTAuthorizationStatusKey: @(actual)
                                                               }];
        completion(LTOperationConditionResultFailed, error);
    } else {
        completion(LTOperationConditionResultSatisfied, nil);
    }
}

@end
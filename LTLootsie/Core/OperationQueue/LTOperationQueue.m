//  LTOperationQueue.m
//  Created by Fabio Teles on 7/15/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTOperationQueue.h"
#import "LTOperation.h"
#import "LTBlockObserver.h"
#import "LTExclusivityController.h"

static void * LTOperationQueueFinishObserverContext = &LTOperationQueueFinishObserverContext;

@implementation LTOperationQueue

- (instancetype)init {
    if (self = [super init]) {
        [self addObserver:self forKeyPath:@"operationCount" options:NSKeyValueObservingOptionNew context:LTOperationQueueFinishObserverContext];
    }
    return self;
}

- (void)dealloc {
    [self removeObserver:self forKeyPath:@"operationCount" context:LTOperationQueueFinishObserverContext];
}

- (void)addOperation:(NSOperation *)operation {
    __weak __typeof(self) weakSelf = self;
    
    if ([operation isKindOfClass:[LTOperation class]]) {
        LTOperation *op = (LTOperation *)operation;
        
        // Set up a `BlockObserver` to invoke the `OperationQueueDelegate` method.
        LTBlockObserver *observer = [[LTBlockObserver alloc] initWithStartHandler:nil produceHandler:^(LTOperation *operation, NSOperation *newOperation) {
            [weakSelf addOperation:newOperation];
        } finishHandler:^(LTOperation *operation, NSArray *errors) {
            [weakSelf.delegate operationQueue:weakSelf operationDidFinish:operation withErrors:errors];
        }];
        
        [op addObserver:observer];
        
        // Extract any dependencies needed by this operation.
        [op.conditions enumerateObjectsUsingBlock:^(NSObject <LTOperationCondition> *condition, NSUInteger idx, BOOL *stop){
            NSOperation *dependency = [condition dependencyForOperation:op];
            if (dependency) {
                [op addDependency:dependency];
                [self addOperation:dependency];
            }
        }];
        
        /*
         With condition dependencies added, we can now see if this needs
         dependencies to enforce mutual exclusivity.
         */
        
        NSMutableArray * concurrencyCategories = [NSMutableArray array];
        
        [op.conditions enumerateObjectsUsingBlock:^(NSObject <LTOperationCondition> *condition, NSUInteger idx, BOOL *stop){
            if (![condition isMutuallyExclusive]){
                return;
            }
            [concurrencyCategories addObject:[condition name]];
        }];
        
        if (concurrencyCategories.count > 0) {
            // Set up the mutual exclusivity dependencies.
            LTExclusivityController * exclusivityController = [LTExclusivityController sharedExclusivityController];
            
            [exclusivityController addOperation:op categories:concurrencyCategories];
            
            LTBlockObserver * observer = [[LTBlockObserver alloc] initWithStartHandler:nil
                                                                        produceHandler:nil
                                                                         finishHandler:^(LTOperation * oper, NSArray * error) {
                                                                             [exclusivityController removeOperation:oper categories:concurrencyCategories];
                                                                         }];
            
            [op addObserver:observer];
        }
        
        /*
         Indicate to the operation that we've finished our extra work on it
         and it's now it a state where it can proceed with evaluating conditions,
         if appropriate.
         */
        [op willEnqueue];
        
    } else {
        /*
         For regular `NSOperation`s, we'll manually call out to the queue's
         delegate we don't want to just capture "operation" because that
         would lead to the operation strongly referencing itself and that's
         the pure definition of a memory leak.
         */
        __weak __typeof(operation) weakOperation = operation;
        [operation addCompletionBlock:^{
            [weakSelf.delegate operationQueue:weakSelf operationDidFinish:weakOperation withErrors:@[]];
        }];
    }
    
    [self.delegate operationQueue:self willAddOperation:operation];
    
    [super addOperation:operation];
}

- (void)addOperations:(NSArray *)operations waitUntilFinished:(BOOL)wait {
    /*
     The base implementation of this method does not call `addOperation()`,
     so we'll call it ourselves.
     */
    for (NSOperation *operation in operations) {
        [self addOperation:operation];
    }
    
    if (wait) {
        for (NSOperation *operation in operations) {
            [operation waitUntilFinished];
        }
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:@"operationCount"] && [self isEqual:object] && context == LTOperationQueueFinishObserverContext) {
        NSUInteger newValue = [change[NSKeyValueChangeNewKey] unsignedIntegerValue];
        
        if (newValue == 0) {
            [self.delegate operationQueueDidFinishAllCurrentTasks:self];
        }
    }
}

@end

//  LTBackgroundObserver.m
//  Created by Fabio Teles on 8/17/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTBackgroundObserver.h"

@interface LTBackgroundObserver ()

@property (nonatomic, assign) UIBackgroundTaskIdentifier identifier;
@property (nonatomic, assign, getter=isInBackground) BOOL inBackground;

@end

@implementation LTBackgroundObserver

- (instancetype)init {
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didEnterForeground:) name:UIApplicationDidBecomeActiveNotification object:nil];
        
        _identifier = UIBackgroundTaskInvalid;
        _inBackground = [UIApplication sharedApplication].applicationState == UIApplicationStateBackground;
        
        if (_inBackground) {
            [self startBackgroundTask];
        }
    }
    return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didEnterBackground:(NSNotification *)notification {
    if (!self.isInBackground) {
        self.inBackground = YES;
        [self startBackgroundTask];
    }
}

- (void)didEnterForeground:(NSNotification *)notification {
    if (self.isInBackground) {
        self.inBackground = NO;
        [self endBackgroundTask];
    }
}

- (void)startBackgroundTask {
    if (self.identifier == UIBackgroundTaskInvalid) {
        __weak __typeof(self) weakSelf = self;
        self.identifier = [[UIApplication sharedApplication] beginBackgroundTaskWithName:NSStringFromClass([self class]) expirationHandler:^{
            [weakSelf endBackgroundTask];
        }];
    }
}

- (void)endBackgroundTask {
    if (self.identifier != UIBackgroundTaskInvalid) {
        [[UIApplication sharedApplication] endBackgroundTask:self.identifier];
        self.identifier = UIBackgroundTaskInvalid;
    }
}

#pragma mark - Operation Observer

- (void)operationDidStart:(LTOperation *)operation {
    // no op
}

- (void)operation:(LTOperation *)operation didProduceOperation:(NSOperation *)newOperation {
    // no op
}

- (void)operationDidFinish:(LTOperation *)operation withErros:(NSArray *)erros {
    [self endBackgroundTask];
}

@end

//  LTTimeoutObserver.m
//  Created by Fabio Teles on 7/30/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTTimeoutObserver.h"

NSString * const LTTimeoutKey = @"Timeout";

@interface LTTimeoutObserver ()

@property (nonatomic, readwrite, assign) NSTimeInterval timeout;

@end

@implementation LTTimeoutObserver

- (instancetype)initWithTimeout:(NSTimeInterval)timeout {
    if (self = [super init]) {
        _timeout = timeout;
    }
    return self;
}

- (void)operationDidStart:(LTOperation *)operation {
    // When the operation starts, queue up a block to cause it to time out.
    dispatch_time_t when = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(self.timeout * NSEC_PER_SEC));
    dispatch_after(when, dispatch_get_global_queue(QOS_CLASS_DEFAULT, 0), ^{
        /**
         Cancel the operation if it hasn't finished and hasn't already
         been cancelled.
         */
        if (!operation.finished && !operation.cancelled) {
            NSError *error = [NSError executionErrorWithUserInfo:@{LTTimeoutKey: @(self.timeout)}];
            [operation cancelWithError:error];
        }
    });
}

- (void)operation:(LTOperation *)operation didProduceOperation:(NSOperation *)newOperation {
    // No op
}

- (void)operationDidFinish:(LTOperation *)operation withErros:(NSArray *)erros {
    // No op
}

@end

//  LTURLSessionTaskOperation.h
//  Created by Fabio Teles on 7/14/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTOperation.h"

typedef NSURLRequest *(^LTRequestBuilder)(NSError *__autoreleasing *error);

@class LTNetworkManager;
@interface LTDataTaskOperation : LTOperation

@property (nonatomic, readonly, strong) NSURLSessionDataTask *task;
@property (nonatomic, readonly, strong) NSURLResponse *response;
@property (nonatomic, readonly, strong) id responseObject;
@property (nonatomic, strong) NSURLRequest *request;

/**
 Creates a DataTask operation with a requestBuilder block. A block is used here to
 perform a lazy request build, allowing others operations to occur and retrieve data
 before it's used here.
 */
- (instancetype)initWithRequestBuilder:(LTRequestBuilder)requestBuilder;

/**
 Helper method that returns the HTTP method used by this data task operation if the
 request has been built, otherwise returns nil.
 */
- (NSString *)HTTPMethod;

/**
 Called right after data task finishes. Gives a chance to subclasses to perform any
 further parsing on the responseObject and even generate a new error. Original 
 implementation just returns nil.
 */
- (NSError *)dataTaskDidFinishSuccessfully;

/**
 Gives subclasses a chance to format errors coming from the network. The basic
 implementation checkes for a common pattern returned from Lootsie's API and does a 
 very basic formatting, so if you do any transformation there is no need to call super.
 */
- (NSError *)formatDataTaskError:(NSError *)error;

/**
 Utility method to build a dictionary with the required authentication info. This method
 will use LTNetworkManager's authDataSource to fill up this dictionary.
 */
+ (NSDictionary *)authHeadersIncludingAppSecret:(BOOL)appSecret
                                     apiSession:(BOOL)apiSession
                                    userSession:(BOOL)userSession;

@end

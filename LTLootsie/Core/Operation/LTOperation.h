//  LTOperation.h
//  Created by Fabio Teles on 7/14/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <Foundation/Foundation.h>

extern NSString * const LTOperationKey;

@class LTOperation;
@protocol LTOperationObserver <NSObject>

@required
/// Invoked immediately prior to the `Operation`'s `execute()` method.
- (void)operationDidStart:(LTOperation *)operation;

/// Invoked when `Operation.produceOperation(_:)` is executed.
- (void)operation:(LTOperation *)operation didProduceOperation:(NSOperation *)newOperation;

/**
 Invoked as an `Operation` finishes, along with any errors produced during
 execution (or readiness evaluation).
 */
- (void)operationDidFinish:(LTOperation *)operation withErros:(NSArray *)erros;

@end

#pragma mark -

typedef NS_ENUM(NSUInteger, LTOperationConditionResult) {
    LTOperationConditionResultSatisfied,
    LTOperationConditionResultFailed
};

extern NSString * const LTOperationConditionKey;

@class LTOperation;
@protocol LTOperationCondition <NSObject>

/**
 The name of the condition. This is used in userInfo dictionaries of `.ConditionFailed`
 errors as the value of the `OperationConditionKey` key.
 */
- (NSString *)name;

/**
 Specifies whether multiple instances of the conditionalized operation may
 be executing simultaneously.
 */
- (BOOL)isMutuallyExclusive;

/**
 Some conditions may have the ability to satisfy the condition if another
 operation is executed first. Use this method to return an operation that
 (for example) asks for permission to perform the operation
 
 - parameter operation: The `Operation` to which the Condition has been added.
 - returns: An `NSOperation`, if a dependency should be automatically added. Otherwise, `nil`.
 - note: Only a single operation may be returned as a dependency. If you
 find that you need to return multiple operations, then you should be
 expressing that as multiple conditions. Alternatively, you could return
 a single `GroupOperation` that executes multiple operations internally.
 */
- (NSOperation *)dependencyForOperation:(LTOperation *)operation;

/// Evaluate the condition, to see if it has been satisfied or not.
- (void)evaluateForOperation:(LTOperation *)operation completion:(void (^)(LTOperationConditionResult result, NSError *error))completion;

@end

#pragma mark -

@interface LTOperation : NSOperation

@property (nonatomic, assign) BOOL userInitiated;

@property (nonatomic, readonly) NSArray *conditions;
@property (nonatomic, readonly) NSArray *observers;
@property (nonatomic, readonly) NSArray *generatedErrors;

- (void)addCondition:(id<LTOperationCondition>)condition;
- (void)addObserver:(id<LTOperationObserver>)observer;

- (void)willEnqueue;

/**
 `execute` is the entry point of execution for all `LTOperation` subclasses.
 If you subclass `Operation` and wish to customize its execution, you would
 do so by overriding the `execute` method.
 
 At some point, your `LTOperation` subclass must call one of the "finish"
 methods defined below; this is how you indicate that your operation has
 finished its execution, and that operations dependent on yours can re-evaluate
 their readiness state.
 */
- (void)execute;

/**
 Cancel the operation manually passing along the error.
 */
- (void)cancelWithError:(NSError *)error;

/**
 Cancel the operation manually passing along the errors.
 */
- (void)cancelWithErrors:(NSArray *)errors;

/**
 Most operations may finish with a single error, if they have one at all.
 This is a convenience method to simplify calling the actual `finish: nil`
 method. This is also useful if you wish to finish with an error provided
 by the system frameworks.
 */
- (void)finishWithError:(NSError *)error;
- (void)finish:(NSArray *)errors;

- (void)produceOperation:(NSOperation *)operation;

/**
 Subclasses may override `finished:` if they wish to react to the operation
 finishing with errors. For example, a `LoadModelOperation` may implement
 this method to potentially inform the user about an error when trying to
 bring up the Core Data stack.
 */
- (void)finished:(NSArray *)errors;

/**
 A backward compatible way to set the operation's name which won't do anything
 on system versions prior to 8.0
 */
- (void)setSafeName:(NSString *)name;

@end

#pragma mark -

@interface LTOperationConditionEvaluator : NSObject

+ (void)evaluate:(NSArray *)conditions operation:(LTOperation *)operation completion:(void (^)(NSArray *errors))completion;

@end

#pragma mark -

@interface NSOperation (LTOperation)

- (void)addCompletionBlock:(void (^)(void))block;

- (void)addDependencies:(NSArray *)dependencies;

@end

#pragma mark -

typedef NS_ENUM(NSInteger, LTOperationErrorCode) {
    LTOperationErrorCodeExecution = -11,
    LTOperationErrorCodeCondition = -12
};

extern NSString * const LTOperationErrorDomain;

@interface NSError (LTOperation)

+ (NSError *)executionErrorWithUserInfo:(NSDictionary *)userInfo;
+ (NSError *)conditionErrorWithUserInfo:(NSDictionary *)userInfo;

@end

//  LTConstants.h
//  Created by Fabio Teles on 7/7/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <Foundation/Foundation.h>
@import UIKit;

@interface LTConstants : NSObject

///////////////////////////////////////////
// SERVER PATHS AND KEYS
///////////////////////////////////////////

extern NSString * const LTBaseStagingAPIPath;
extern NSString * const LTBaseLiveAPIPath;

extern NSString * const LTWebsitePath;
extern NSString * const LTTermsURLComponent;

extern NSString * const LTHeaderAppSecretKey;
extern NSString * const LTHeaderAPISessionTokenKey;
extern NSString * const LTHeaderUserSessionTokenKey;

///////////////////////////////////////////
// GLOBAL STUFF
///////////////////////////////////////////

extern NSString * const LTPlatform;
extern NSString * const LTAPIVersion;
extern NSString * const LTSDKVersion;
extern NSString * const LTLocalizationTable;

///////////////////////////////////////////
// KEY VALUE CODING
///////////////////////////////////////////

extern NSInteger const LTDataStorageVersion;
extern NSString * const LTDataStorageVersionKey;

extern NSString * const LTDatabaseStorageFolder;
extern NSString * const LTDatabaseStorageFilename;
extern NSString * const LTDatabaseStorageOldFolder;
extern NSString * const LTDatabaseStorageOldFilename;
extern NSString * const LTDatabaseStorageOldClassName;
extern NSString * const LTDatabaseStoregeOldDataKey;

extern NSString * const LTAppSecretKey;
extern NSString * const LTAPISessionTokenKey;
extern NSString * const LTUserSessionTokenKey;
extern NSString * const LTLastSessionStartDateKey;
extern NSString * const LTLastSessionEndDateKey;

///////////////////////////////////////////
// USER ACTIVITY
///////////////////////////////////////////

extern NSString * const LTUserActivityMetricSessionStartTime;
extern NSString * const LTUserActivityMetricSessionEndTime;
extern NSString * const LTUserActivityMetricAchievementIANTapped;

///////////////////////////////////////////
// COLORS
///////////////////////////////////////////

extern NSUInteger const LTMainColor;
extern NSUInteger const LTDisabledMainColor;

///////////////////////////////////////////
// JSON API KEYS
///////////////////////////////////////////

extern NSString * const LTAPIDateFormat;
extern NSString * const LTAPIDateOnlyFormat;

extern NSString * const LTIdKey;
extern NSString * const LTNameKey;
extern NSString * const LTDescriptionKey;
extern NSString * const LTPointsKey;
extern NSString * const LTTotalPointsKey;
extern NSString * const LTIsAchievedKey;
extern NSString * const LTRepeatableKey;
extern NSString * const LTRewardsKey;
extern NSString * const LTVideoOfferIDKey;
extern NSString * const LTVideoAchivmentIDKey;

extern NSString * const LTAPIVersionKey;
extern NSString * const LTLatitudeLongitudeKey;
extern NSString * const LTPlatformKey;
extern NSString * const LTDeviceKey;
extern NSString * const LTFirmwareKey;
extern NSString * const LTLanguageKey;
extern NSString * const LTCountryKey;

extern NSString * const LTImageSizeDetailKey;
extern NSString * const LTImageSizeSKey;
extern NSString * const LTImageSizeMKey;
extern NSString * const LTImageSizeLKey;
extern NSString * const LTImageSizeXLKey;

extern NSString * const LTBrandNameKey;
extern NSString * const LTEngagementsKey;
extern NSString * const LTImageUrlsKey;
extern NSString * const LTIsLimitedTimeKey;
extern NSString * const LTIsNewKey;
extern NSString * const LTRedemptionsRemainingKey;
extern NSString * const LTTextToShareKey;
extern NSString * const LTTosTextKey;
extern NSString * const LTTosUrlKey;

extern NSString * const LTEmailKey;
extern NSString * const LTFirstNameKey;
extern NSString * const LTLastNameKey;
extern NSString * const LTGenderKey;
extern NSString * const LTBirthdateKey;
extern NSString * const LTAddressKey;
extern NSString * const LTCityKey;
extern NSString * const LTStateKey;
extern NSString * const LTZipcodeKey;
extern NSString * const LTPhotoURLKey;
extern NSString * const LTConfirmedAgeKey;
extern NSString * const LTHomeZipcodeKey;
extern NSString * const LTInterestGroupsKey;
extern NSString * const LTAcceptedTOSKey;
extern NSString * const LTLootsieOptinKey;
extern NSString * const LTPartnerOptinKey;
extern NSString * const LTIsGuestKey;

extern NSString * const LTAchievedPointsKey;
extern NSString * const LTAchievablePointsKey;
extern NSString * const LTTotalPointsEarnedKey;
extern NSString * const LTAchievementsKey;
extern NSString * const LTDurationKey;
extern NSString * const LTGenresKey;
extern NSString * const LTHookTypeKey;
extern NSString * const LTMoreAppsKey;
extern NSString * const LTNotificationKey;
extern NSString * const LTTextColorKey;
extern NSString * const LTBackgroundColorKey;
extern NSString * const LTPosXKey;
extern NSString * const LTPosYKey;
extern NSString * const LTOrientationKey;

extern NSString * const LTColorRKey;
extern NSString * const LTColorGKey;
extern NSString * const LTColorBKey;

extern NSString * const LTEtagKey;
extern NSString * const LTSettingsKey;
extern NSString * const LTCurrencyNameKey;
extern NSString * const LTSDKEnabledKey;
extern NSString * const LTCurrencyAbbreviationKey;
extern NSString * const LTRewardsPageSizeKey;

extern NSString * const LTMetricKey;
extern NSString * const LTValueKey;
extern NSString * const LTTimeKey;

extern NSString * const LTRewardIdsKey;
extern NSString * const LTAchievementIdsKey;
extern NSString * const LTRewardRedemptionIdKey;

extern NSString * const LTErrorErrorsKey;
extern NSString * const LTErrorFieldKey;
extern NSString * const LTErrorMessageKey;

@end

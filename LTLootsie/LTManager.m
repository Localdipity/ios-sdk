//  LTManager.m
//  Created by Fabio Teles on 7/30/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTManager.h"
#import "LTManager+Private.h"

#import "LTNetworkManager.h"
#import "LTAllSequence.h"

#import "LTBlockOperation.h"
#import "LTPostNotificationOperation.h"
#import "LTNoCancelledDependencies.h"

#import "LTApp.h"
#if ENABLE_MARKETPLACE
#import "LTReward.h"
#endif
#if ENABLE_INTERSTITIAL
#import "LTBaseInterstitialReward.h"
#endif
#if ENABLE_ACCOUNT
#import "LTUserAccount.h"
#endif
#import "LTUserActivity.h"

#import "LTDatabaseProtocol.h"
#import "LTDatabase.h"
#import "LTData.h"

#import "LTTimer.h"
#import "LTMWLogging.h"
#import "LTAFNetworkReachabilityManager.h"

NSString * const LTInitializationCompleteNotification   = @"LTInitializationCompleteNotification";
NSString * const LTUserAccountUpdatedNotification       = @"LTUserAccountUpdatedNotification";
NSString * const LTAchievementReachedNotification       = @"LTAchievementReachedNotification";
NSString * const LTVideoInterstitialWathcedNotification = @"LTVideoInterstitialWathcedNotification";
NSString * const LTRewardRedeemedNotification           = @"LTRewardRedeemedNotification";

NSString * const LTAchievementUserInfoKey               = @"achievement";
NSString * const LTSavedInfoUserInfoKey                 = @"savedInfo";
NSString * const LTRewardUserInfoKey                    = @"reward";
NSString * const LTUserUserInfoKey                      = @"user";

NSInteger const LTSDKFailedInitializingErrorCode        = -101;
NSString * const LTErrorDomain                          = @"com.lootsie.error";

NSString * const LTManagerGeneralQueueName              = @"com.lootsie.manager.general.queue";

void * LTKVOContext = &LTKVOContext;

#pragma mark -

@interface LTManager ()

@property (nonatomic, strong) id <LTDatabaseProtocol>database;

@property (nonatomic, copy) NSString *appKey;
@property (nonatomic, strong) NSString *loginEmail;
@property (nonatomic, assign) BOOL sendLocation;
@property (nonatomic, assign) BOOL hasPendingOperations;

@property (nonatomic, strong) LTTimer *saveTimer;
@property (nonatomic, strong) LTSuccessBlock lazyStartSuccessBlock;
@property (nonatomic, strong) LTFailureBlock lazyStartFailureBlock;

@end

@implementation LTManager {
    BOOL _lazyInitializationDisabled;
}

@dynamic data;

+ (instancetype)sharedManager {
    static LTManager *_engine;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _engine = [self new];
    });
    return _engine;
}

- (instancetype)init {
    if (self = [super init]) {
        _status = LTSDKStatusNotInitialized;
        
        // general queue
        _generalQueue = [LTOperationQueue new];
        _generalQueue.name = LTManagerGeneralQueueName;
        _generalQueue.delegate = self;
        
        // op to be triggered after init
        
        // database
        _database = [LTDatabase new];

        // network manager (network queue delegate and auth source)
        [LTNetworkManager lootsieManager].networkQueue.delegate = self;
        [LTNetworkManager lootsieManager].authDataSource = _database.data;
        
        // KVO
        [_database.data addObserver:self
                         forKeyPath:@"user"
                            options:(NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew)
                            context:LTKVOContext];
        
        // Notifications
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
        
        // Record session begin
        [self handleSessionBegin];
    }
    return self;
}

- (void)dealloc {
    // Notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    
    // KVO
#if ENEBLE_ACCOUNT
    [self.data.user removeObserver:self forKeyPath:@"info" context:LTKVOContext];
    [self.data removeObserver:self forKeyPath:@"user" context:LTKVOContext];
#endif
}
#pragma mark - Status management
- (void)observeConnectivityToReset {
    __weak LTAFNetworkReachabilityManager *reachabilityManager = [LTNetworkManager lootsieManager].reachabilityManager;
    if (![reachabilityManager isReachable]) {
        [reachabilityManager setReachabilityStatusChangeBlock:^(LTAFNetworkReachabilityStatus status) {
            if (reachabilityManager.isReachable) {
                [self resetSDKStatus];
                [reachabilityManager setReachabilityStatusChangeBlock:nil];
            }
        }];
    }
}

- (void)setStatus:(LTSDKStatus)status {
    NSAssert(status != _status, @"Performing invalid cyclic status transition");
    NSAssert(status > _status, @"Performing invalid out of order status change");
    
    // cannot leave ready or failed status
    if (_status != LTSDKStatusReady && _status != LTSDKStatusFailed) {
        _status = status;
    }
}

- (void)resetSDKStatus {
    NSAssert(self.status == LTSDKStatusFailed, @"SDK MUST be in state Failed in order to reset it");
    
    if (self.status != LTSDKStatusFailed) {
        LTLogWarning(@"Trying to reset the SDK when it's not is the failed state");
        return;
    }
    
    // re-create the after init op
    [self createAfterOperation];
    
    // reset status
    [self willChangeValueForKey:@"status"];
    _status = LTSDKStatusNotInitialized;
    [self didChangeValueForKey:@"status"];
}

#pragma mark -
- (void)disableLazyInitialization {
    NSAssert(self.status <= LTSDKStatusWaitingInitialization, @"You can not disable lazy initialization after initializing it");
    _lazyInitializationDisabled = YES;
    if (self.status == LTSDKStatusWaitingInitialization) {
        [self startIfNeededWithSuccess:nil failure:nil];
    }
}

- (void)setCustomDatabase:(id<LTDatabaseProtocol>)database {
    NSString *message = @"You can NOT change the database after the initialization process has started";
    NSAssert(self.status <= LTSDKStatusWaitingInitialization, message);
    if (self.status > LTSDKStatusWaitingInitialization) {
        LTLogWarning(@"%@", message);
    } else {
        self.database = database;
    }
}

#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {

    if (context == LTKVOContext) {
#if ENABLE_ACCOUNT
        if ([keyPath isEqualToString:@"user"]) {
            LTUserAccount *oldUser = (LTUserAccount *)[change objectForKeyNilSafe:NSKeyValueChangeOldKey];
            /**
             If the entire object changed remove KVO from old one and add it on
             the new one. Since we're using `objectForKeyNilSafe:` if it's nil there
             is no harm calling methods.
             */
            LTUserAccount *newUser = (LTUserAccount *)[change objectForKeyNilSafe:NSKeyValueChangeNewKey];
            [oldUser removeObserver:self forKeyPath:@"info" context:LTKVOContext];
            [newUser addObserver:self forKeyPath:@"info" options:NSKeyValueObservingOptionInitial context:LTKVOContext];
        }
        if ([keyPath isEqualToString:@"user"] || [keyPath isEqualToString:@"info"]) {
            LTPostNotificationOperation *notifOp = [[LTPostNotificationOperation alloc] initWithNotificationName:LTUserAccountUpdatedNotification
                                                                                                          object:self
                                                                                                        userInfo:@{LTUserUserInfoKey : (self.data.user ?: [NSNull null])}];
            [_generalQueue addOperation:notifOp];
        }
#endif
    }
}

#pragma mark - Notifications

- (void)applicationWillEnterForeground:(NSNotification *)notification {
    [self handleSessionBegin];
}

- (void)applicationDidEnterBackground:(NSNotification *)notification {
    [self handleSessionEnd];
}

#pragma mark - Database
- (void)schedulePeriodicSaveData {
    [self scheduleSaveDataWithInterval:5.0];
}

- (void)scheduleUrgentSaveData {
    [self scheduleSaveDataWithInterval:0.02];
}

- (void)scheduleSaveDataWithInterval:(NSTimeInterval)interval {
    [self.saveTimer cancel];
    __weak __typeof(self) weakSelf = self;
    self.saveTimer = [[LTTimer alloc] initWithInterval:interval handler:^{
        [weakSelf saveData];
    }];
}

- (void)saveData {
    self.saveTimer = nil;
    [self.database save];
}

#pragma mark - Sessions
- (void)handleSessionBegin {
    [[LTNetworkManager lootsieManager].reachabilityManager startMonitoring];
    
    [self recordSessionBegin];
}

- (void)handleSessionEnd {
    [self scheduleUrgentSaveData];
    [self recordSessionEnd];
    
    [[LTNetworkManager lootsieManager].reachabilityManager stopMonitoring];
}

- (void)recordSessionBegin {
    LTUserActivity *sessionBeginActivity = [LTUserActivity userActivityWithMetric:LTUserActivityMetricSessionStartTime value:@"1"];
    self.data.lastSessionStartDate = sessionBeginActivity.time;
    
    LTLogDebug(@"Session starting at: %@", sessionBeginActivity.time);
    [self sendUserActivityWithUserActivity:sessionBeginActivity success:nil failure:nil];
}

- (void)recordSessionEnd {
    LTUserActivity *sessionEndActivity = [LTUserActivity userActivityWithMetric:LTUserActivityMetricSessionEndTime value:@"1"];
    self.data.lastSessionEndDate = sessionEndActivity.time;
    
    LTLogDebug(@"Session ending at: %@", sessionEndActivity.time);
    [self sendUserActivityWithUserActivity:sessionEndActivity success:nil failure:nil];
}

#pragma mark - API calls
- (void)startEngineWithAppKey:(NSString *)appKey
                      success:(void (^)(void))success
                      failure:(void (^)(NSError *))failure {
    // if already initialized do nothing
    if (self.status > LTSDKStatusNotInitialized) {
        LTLogInfo(@"SDK already started");
        return;
    } else {
        self.appKey = appKey;
        [self addLazyStartSuccessBlock:success];
        [self addLazyStartFailureBlock:failure];
        
        // laziness
        if (_lazyInitializationDisabled || self.hasPendingOperations) {
            LTLogInfo(@"Starting SDK");
            [self addStartOperation];
        } else {
            LTLogDebug(@"Saving info for lazy initialization");
            self.status = LTSDKStatusWaitingInitialization;
        }
    }
}

- (void)startIfNeededWithSuccess:(void (^)(void))success
                         failure:(void (^)(NSError *))failure {
    LTBlockOperation *op = [[LTBlockOperation alloc] initWithMainQueueBlock:^{
        if (self.status == LTSDKStatusReady) {
            if (success) success();
        } else {
            if (failure) failure([NSError errorWithDomain:LTNetworkErrorDomain code:LTSDKFailedInitializingErrorCode userInfo:nil]);
        }
    }];
    op.userInitiated = YES;
    [op setSafeName:@"com.lootsie.manager.startIfNeeded"];
    [self addStartPendingNetworkOperation:op];
}

- (void)loginWithEmail:(NSString *)email
               success:(void (^)(void))success
               failure:(void (^)(NSError *))failure {
    
    if (self.status >= LTSDKStatusNotInitialized) {
        LTLoginAndAccountSequenceOperation *loginOp = [LTLoginAndAccountSequenceOperation sequenceWithEmail:email];
        [loginOp setSuccess:success failure:failure];
        [self addStartPendingNetworkOperation:loginOp];
    } else {
        self.loginEmail = email;
        [self addLazyStartSuccessBlock:success];
        [self addLazyStartFailureBlock:failure];
        [self addStartOperation];
    }
    
}

- (void)sendUserLocationWithSuccess:(void (^)(void))success
                            failure:(void (^)(NSError *))failure {
    
    if (self.status > LTSDKStatusWaitingInitialization) {
        LTSendUserLocationSequenceOperation *userLocationOp = [LTSendUserLocationSequenceOperation sequence];
        [self addStartPendingNetworkOperation:userLocationOp];
    } else {
        self.sendLocation = YES;
        [self addLazyStartSuccessBlock:success];
        [self addLazyStartFailureBlock:failure];
        [self addStartOperation];
    }
}

- (void)sendUserActivityWithMetric:(NSString *)metric
                             value:(NSString *)value
                        activityId:(NSString *)activityId
                           success:(void (^)(void))success
                           failure:(void (^)(NSError *error))failure {
    [self sendUserActivityWithUserActivity:[LTUserActivity userActivityWithMetric:metric value:value activityId:activityId] success:success failure:failure];
}

- (void)sendUserActivityWithUserActivity:(LTUserActivity *)userActivity
                                 success:(void (^)(void))success
                                 failure:(void (^)(NSError *error))failure {
    LTSendUserActivitySequenceOperation *activityOp = [LTSendUserActivitySequenceOperation sequenceWithUserActivity:userActivity];
    [activityOp setSuccess:success failure:failure];
    [self addStartPendingNetworkOperation:activityOp startIfPossible:NO];
}

- (void)logoutWithSuccess:(void (^)(void))success failure:(void (^)(NSError *))failure {
    if (self.status == LTSDKStatusReady) {
        LTLogoutUserSequenceOperation *logoutSequence = [LTLogoutUserSequenceOperation sequence];
        [logoutSequence setSuccess:success failure:failure];
        [self addNetworkOperation:logoutSequence];
    } else {
        LTLogDebug(@"Cannot logout if the SDK is not at ready state");
    }
}

#pragma mark - Easy access to data
- (LTData *)data {
    return self.database.data;
}
#pragma mark - Queue delegate
- (void)operationQueue:(LTOperationQueue *)queue operationDidFinish:(NSOperation *)operation withErrors:(NSArray *)errors {
    if ([operation isKindOfClass:[LTInitSequenceOperation class]]) {
        if (operation.isCancelled) {
            NSError *error = errors.firstObject;
            if (error.code == NSURLErrorNotConnectedToInternet ||
                error.code == NSURLErrorTimedOut ||
                error.code == NSURLErrorNetworkConnectionLost ||
                error.code == NSURLErrorDataNotAllowed) {
                [self observeConnectivityToReset];
            }
        } else {
            [self schedulePeriodicSaveData];
        }
    }
    
    if ([operation isKindOfClass:[LTLogoutUserSequenceOperation class]]) {
        [self schedulePeriodicSaveData];
    }
}

- (void)operationQueue:(LTOperationQueue *)queue willAddOperation:(NSOperation *)operation {
    // no op
}

- (void)operationQueueDidFinishAllCurrentTasks:(LTOperationQueue *)queue {
    // no op
}

@end

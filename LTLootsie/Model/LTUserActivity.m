//  LTUserActivity.m
//  Created by Fabio Teles on 7/10/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTConstants.h"
#import "LTUserActivity.h"
#import "LTTools.h"

@implementation LTUserActivity

+ (instancetype)userActivityWithMetric:(NSString *)metric value:(NSString *)value {
    return [self userActivityWithMetric:metric value:value activityId:nil time:nil];
}

+ (instancetype)userActivityWithMetric:(NSString *)metric value:(NSString *)value activityId:(NSString *)activityId {
    return [self userActivityWithMetric:metric value:value activityId:activityId time:nil];
}

+ (instancetype)userActivityWithMetric:(NSString *)metric value:(NSString *)value activityId:(NSString *)activityId time:(NSDate *)time {
    return [[self alloc] initWithMetric:metric value:value activityId:activityId time:time];
}

- (instancetype)initWithMetric:(NSString *)metric value:(NSString *)value activityId:(NSString *)activityId time:(NSDate *)time {
    NSParameterAssert(metric);
    NSParameterAssert(value);
    
    if (self = [super init]) {
        _metric = metric;
        _value = value;
        _activityId = activityId;
        _time = (time ? : [NSDate date]);
    }
    return self;
}

- (void)populateWithDictionary:(NSDictionary *)dictionary
{
    self.activityId = [dictionary objectForKeyNilSafe:LTIdKey];
    self.metric = [dictionary objectForKeyNilSafe:LTMetricKey];
    self.value = [dictionary objectForKeyNilSafe:LTValueKey];
    
    NSTimeInterval timeInterval = [[dictionary objectForKeyNilSafe:LTTimeKey] doubleValue];
    self.time = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    
}

- (NSDictionary *)dictionary {
    NSMutableDictionary *mutableDic = [@{LTMetricKey    : self.metric,
                                         LTValueKey     : self.value,
                                         LTTimeKey      : @([self.time timeIntervalSince1970])} mutableCopy];
    
    if (self.activityId) mutableDic[LTIdKey] = self.activityId;
    
    return [NSDictionary dictionaryWithDictionary:mutableDic];
}

@end

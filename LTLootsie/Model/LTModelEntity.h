//  ModelEntity.h
//  Created by fabio teles on 10/12/12.
//  Copyright (c) 2012 pixel4. Copyright All Rights Reserved (http://pixel4.co)

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.


#import "LTConstants.h"
#import <Foundation/Foundation.h>

@interface LTModelEntity : NSObject

+ (instancetype)entityWithDictionary:(NSDictionary *)dictionary;
+ (NSArray *)collectionWithArray:(NSArray *)array;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

- (void)populateWithDictionary:(NSDictionary *)dictionary;

- (NSDictionary *)dictionary;

@end

@interface NSDictionary (LTModelEntity)

- (id)objectForKeyNilSafe:(id)aKey;

@end

@interface NSArray (LTModelEntity)

- (NSArray *)collectionOfDictionaries;

@end
//  LTManager.h
//  Created by Fabio Teles on 7/30/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <Foundation/Foundation.h>
#import "LTOperationQueue.h"

extern NSString * const LTInitializationCompleteNotification;
extern NSString * const LTUserAccountUpdatedNotification;
extern NSString * const LTAchievementReachedNotification;
extern NSString * const LTVideoInterstitialWathcedNotification;
extern NSString * const LTRewardRedeemedNotification;

extern NSString * const LTAchievementUserInfoKey;
extern NSString * const LTSavedInfoUserInfoKey;
extern NSString * const LTRewardUserInfoKey;
extern NSString * const LTUserUserInfoKey;

extern NSString * const LTErrorDomain;
extern NSInteger const LTSDKFailedInitializingErrorCode;

typedef NS_ENUM(NSUInteger, LTSDKStatus) {
    LTSDKStatusNotInitialized = 0,
    LTSDKStatusWaitingInitialization,
    LTSDKStatusInitializing,
    LTSDKStatusReady,
    LTSDKStatusFailed
};

@class LTUserAccount, LTData, LTBlockOperation;
@protocol LTDatabaseProtocol;
/*!
 * @class LTManager
 * LTManager singleton class. This class simplifies server interaction and stores Lootsie loyalty data.
 * All callbacks are implemented in blocks and are called async
 */
@interface LTManager : NSObject <LTOperationQueueDelegate> {
    LTOperationQueue *_generalQueue;
}

/*!
 * Return the data object in memory for easy access
 */
@property (nonatomic, readonly) LTData *data;

/*!
 *Current status of the SDK. See LTSDKStatus
 */
@property (nonatomic, readonly, assign) LTSDKStatus status;

/*!
 *
 *  @return Returns the LTManager object.
 */
+ (instancetype)sharedManager;

/*!
 * Calling this method will disable the lazy initialization which is by default On. It only
 * affects the behavior if called before initialization is called or if initialization is
 * waiting to occur, in this case it'll trigger initialization immediatelly.
 */
- (void)disableLazyInitialization;

/*!
 * You can optionally define a custom database class that implements the LTDatabaseProtocol
 * and set it here before initializing the SDK. If you try to call this method after
 * initializing nothing will occur and an error message will be logged. If not set,
 * by default an instance of LTDatabase class will be used as database.
 */
- (void)setCustomDatabase:(id<LTDatabaseProtocol>)database;

/*!
 * @discussion installs Lootsie app identificator.
 * launches session start if lazy initialization is disabled or there are requests to server.
 * @param appKey  - app identificator that is obtained in dashboard
 * @param success callback is called after successful session start
 * @param error callback is called when server session failed to start
 */
- (void)startEngineWithAppKey:(NSString *)appKey
                      success:(void (^)(void))success
                      failure:(void (^)(NSError *error))failure;
/*!
 * @discussion starts session for server interaction if needed
 * @param success callback  about sucessful server session start
 * @param error callback  that is called when srver session failed to start
 */
- (void)startIfNeededWithSuccess:(void (^)(void))success
                         failure:(void (^)(NSError *error))failure;

/*!
 * Method is called to merge points of guest user and existing registered user
 * @param email email of registered user
 * @param success Callback about successful points merge
 * @param failure Callback about unsuccessful points merge
 */
- (void)loginWithEmail:(NSString *)email
               success:(void (^)(void))success
               failure:(void (^)(NSError *error))failure;

/*!
 *  Sends user location to server
 *
 *  @param success Callback about successful user location data update
 *  @param failure Callback is called when system failed to send user location to server
 */
- (void)sendUserLocationWithSuccess:(void (^)(void))success
                            failure:(void (^)(NSError *error))failure;

/*!
 *  Sends user activity to server for analytics
 *
 *  @param metric     event expired
 *  @param value      event value
 *  @param activityId identificator for activity
 *  @param success    callback about successful analytics sending to server
 *  @param failure    callback about failure to update user activity
 */
- (void)sendUserActivityWithMetric:(NSString *)metric
                             value:(NSString *)value
                        activityId:(NSString *)activityId
                           success:(void (^)(void))success
                           failure:(void (^)(NSError *error))failure;

/*!
 * Method is called for user logout
 * @param success Callback about successful logout
 * @param failure Callback that is called when logout failed
 */
- (void)logoutWithSuccess:(void (^)(void))success
                  failure:(void (^)(NSError *error))failure;

@end

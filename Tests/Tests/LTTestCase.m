//  LootsieTests.m
//  Created by Fabio Teles on 7/22/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTTestCase.h"
#import "LTOperation.h"
#import "LTOperationQueue.h"

NSString * const LTUserEmailTest        = @"hildgarden@mail.ru";//@"lootsietest@lootsie.com";
NSString * const LTAchievementIdTest    = @"back1";
NSString * const LTActivityMetricTest   = @"testActivityMetricId";
NSString * const LTActivityIdTest       = nil;
NSInteger const  LTOfferIDTest          = 5;

@interface LTTestCase ()

@property (nonatomic, strong) LTOperationQueue *testQueue;

@end

@implementation LTTestCase

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    [Expecta setAsynchronousTestTimeout:15.0];
    self.testQueue = [LTOperationQueue new];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [self.testQueue cancelAllOperations];
    self.testQueue =  nil;
    [super tearDown];
}

- (void)addTestOperation:(LTOperation *)operation {
    [self.testQueue addOperation:operation];
}

@end

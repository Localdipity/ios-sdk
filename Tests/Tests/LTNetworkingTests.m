//  LTNetworkingTests.m
//  Created by Fabio Teles on 7/22/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTTestCase.h"

#import "LTLootsie.h"

#import "LTAllOperation.h"
#import "LTAllSequence.h"

#import "LTManager.h"
#import "LTReward.h"
#import "LTNetworkManager.h"
#import "LTBlockOperation.h"
#import "LTBlockObserver.h"
#import "LTPostUserAchievementsOperation.h"
#import "LTGetUserAchievementsOperation.h"
#import "LTVideoRewardInterstitialOperation.h"
#import "LTIANInterstitialOperation.h"

#import "LTTools.h"
#import "LTHelperUnitTest.h"

@interface LTNetworkingTests : LTTestCase

@end

@implementation LTNetworkingTests

- (void)tearDown {
    // [[LTNetworkManager lootsieManager] invalidateSessionCancelingTasks:YES];
    [super tearDown];
}

#pragma mark - Operations

- (void)testPOSTApiSessionOperation {
    LTApiSessionOperation *op = [LTApiSessionOperation POSTWithAppSecret:[LTHelperUnitTest LTAppSecretTest] locationBlock:nil];
    [self addTestOperation:op];
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
    expect(op.apiSessionToken).notTo.beNil();
}

- (void)testPOSTUserSessionAsGuestOperation {
    [self loginAsGuestUser:YES];
    [LTManager sharedManager].data.userSessionToken = nil;
    
    LTUserSessionOperation *op = [LTUserSessionOperation POSTAsGuest];
    [self addTestOperation:op];
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
    expect(op.userSessionToken).notTo.beNil();
}

- (void)testPOSTUserSessionWithEmailOnlyOperation {
    [self loginAsGuestUser:YES];
    [LTManager sharedManager].data.userSessionToken = nil;
    
    LTUserSessionOperation *guestOp = [LTUserSessionOperation POSTAsGuest];
    LTBlockOperation *blockOp = [LTBlockOperation autoSuccessOpWithBlock:^{ [LTManager sharedManager].data.userSessionToken = guestOp.userSessionToken; }];
    LTUserSessionOperation *op = [LTUserSessionOperation POSTWithEmailOnly:LTUserEmailTest];
    
    [blockOp addDependency:guestOp];
    [op addDependency:guestOp];
    
    [self addTestOperation:guestOp];
    [self addTestOperation:blockOp];
    [self addTestOperation:op];
    
    expect(guestOp.isFinished).will.beTruthy();
    expect(guestOp.generatedErrors.count).equal(0);
    expect(guestOp.userSessionToken).notTo.beNil();
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
}

- (void)testDELETEUserSessionOperation {
    [self loginAsGuestUser:YES];
    [LTManager sharedManager].data.userSessionToken = nil;
    
    LTUserSessionOperation *guestOp = [LTUserSessionOperation POSTAsGuest];
    LTBlockOperation *blockOp = [LTBlockOperation autoSuccessOpWithBlock:^{ [LTManager sharedManager].data.userSessionToken = guestOp.userSessionToken; }];
    LTUserSessionOperation *op = [LTUserSessionOperation DELETE];
    
    [blockOp addDependency:guestOp];
    [op addDependency:blockOp];
    
    [self addTestOperation:guestOp];
    [self addTestOperation:blockOp];
    [self addTestOperation:op];
    
    expect(guestOp.isFinished).will.beTruthy();
    expect(guestOp.generatedErrors.count).equal(0);
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
}

- (void)testGETAppOperation {
    [self loginAsGuestUser:YES];
    
    LTAppOperation *op = [LTAppOperation GET];
    
    [self addTestOperation:op];
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
    expect(op.app.appId).notTo.beNil();
}

- (void)testGETSettingsOperation {
    [self loginAsGuestUser:YES];
    
    LTSettingsOperation *op = [LTSettingsOperation GET];
    
    [self addTestOperation:op];
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
    expect(op.settings.currencyName).notTo.beNil();
}

- (void)testGETUserAccount {
    [self loginAsUserWithEmail:LTUserEmailTest isNeedResetData:YES];
    
    LTUserAccountOperation *op = [LTUserAccountOperation GET];
    
    [self addTestOperation:op];
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
    expect(op.userAccount.email).notTo.beNil();
}

- (void)testPUTUserAccount {
    [self loginAsUserWithEmail:LTUserEmailTest isNeedResetData:NO];
    LTUserAccount *newAccount = [self userAccountForTest];

    LTUserAccountOperation *op = [LTUserAccountOperation PUTWithUserAccount:newAccount];
    LTUserAccountOperation *confirmOp = [LTUserAccountOperation GET];
    
    [confirmOp addDependency:op];
    
    [self addTestOperation:op];
    [self addTestOperation:confirmOp];
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
    
    expect(confirmOp.isFinished).will.beTruthy();
    expect(confirmOp.generatedErrors.count).equal(0);
    
    expect(confirmOp.userAccount).notTo.beNil();
    
    expect(confirmOp.userAccount.firstName).equal(newAccount.firstName);
    expect(confirmOp.userAccount.lastName).equal(newAccount.lastName);
    expect(confirmOp.userAccount.gender).equal(newAccount.gender);
    expect(confirmOp.userAccount.birthdate).equal(newAccount.birthdate);
    expect(confirmOp.userAccount.address).equal(newAccount.address);
    expect(confirmOp.userAccount.city).equal(newAccount.city);
    expect(confirmOp.userAccount.state).equal(newAccount.state);
    expect(confirmOp.userAccount.zipcode).equal(newAccount.zipcode);
    expect(confirmOp.userAccount.confirmedAge).equal(newAccount.confirmedAge);
    expect(confirmOp.userAccount.acceptedTOS).equal(newAccount.acceptedTOS);
    expect(confirmOp.userAccount.lootsieOptin).equal(newAccount.lootsieOptin);
    expect(confirmOp.userAccount.partnerOptin).equal(newAccount.partnerOptin);
}

- (void)testGETUserAchievementsOperation {
    [self loginAsGuestUser:YES];
    
    LTGetUserAchievementsOperation *op = [LTGetUserAchievementsOperation GET];
    
    [self addTestOperation:op];
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
    expect(op.userAchievements).notTo.beNil();
}

- (void)testPOSTUserAchievementsOperations {
    [self loginAsGuestUser:YES];
    
    LTPostUserAchievementsOperation *op = [LTPostUserAchievementsOperation POSTWithAchievementId:LTAchievementIdTest];
                       
    [self addTestOperation:op];
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
    expect(op.savedInfo).notTo.beNil();
    expect(op.savedInfo.points).notTo.equal(0);
}

- (void)testPOSTUserActivityOperation {
    [self loginAsGuestUser:YES];
    
    LTUserActivityOperation *op = [LTUserActivityOperation POSTWithMetric:LTActivityMetricTest value:@"0" activityId:LTActivityIdTest];
    
    [self addTestOperation:op];
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
}

#pragma mark - Sequences

- (void)testInitSequenceOperationAsGuest {
    LTInitSequenceOperation *op = [LTInitSequenceOperation sequenceAsGuestWithAppSecret:[LTHelperUnitTest LTAppSecretTest] sendLocation:NO];
    [self addTestOperation:op];
   
    expect(op.isFinished).will.beTruthy();
    expect([LTManager sharedManager].data.appSecret).notTo.beNil();
    expect([LTManager sharedManager].data.apiSessionToken).notTo.beNil();
    expect([LTManager sharedManager].data.userSessionToken).notTo.beNil();
    expect(op.generatedErrors.count).equal(0);
    
    LTUserAccountOperation *userOp = [LTUserAccountOperation GET];
    
    [self addTestOperation:userOp];
    
    expect(userOp.isFinished).will.beTruthy();
    expect(userOp.userAccount).notTo.beNil();
    expect(userOp.userAccount.isGuest).to.beTruthy();
}

- (void)testInitSequenceOperationAsUser {
    [LTManager sharedManager].data.appSecret = nil;
    [LTManager sharedManager].data.apiSessionToken = nil;
    [LTManager sharedManager].data.userSessionToken = nil;
    
    LTInitSequenceOperation *op = [LTInitSequenceOperation sequenceWithAppSecret:[LTHelperUnitTest LTAppSecretTest] emailIfNotLogged:LTUserEmailTest sendLocation:NO];
    [self addTestOperation:op];
    
    expect(op.isFinished).will.beTruthy();
    expect([LTManager sharedManager].data.appSecret).notTo.beNil();
    expect([LTManager sharedManager].data.apiSessionToken).notTo.beNil();
    expect([LTManager sharedManager].data.userSessionToken).notTo.beNil();
    expect(op.generatedErrors.count).equal(0);
    
    LTUserAccountOperation *userOp = [LTUserAccountOperation GET];
    
    [self addTestOperation:userOp];
    
    expect(userOp.isFinished).will.beTruthy();
    expect(userOp.userAccount).notTo.beNil();
    expect(userOp.userAccount.isGuest).notTo.beTruthy();
}

- (void)testAchievementSequenceOperation {
    [self loginAsGuestUser:NO];
    
    
    LTAchievementSequenceOperation *op = [LTAchievementSequenceOperation sequenceWithAchievementId:LTAchievementIdTest];
    [self addTestOperation:op];
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
}

- (void)testRedemptionSequenceOpeartion {
    [self loginAsUserWithEmail:LTUserEmailTest isNeedResetData:YES];
    
    LTUserRewardsOperation *rewardsOp = [LTUserRewardsOperation GET];
    [self addTestOperation:rewardsOp];
    
    expect(rewardsOp.isFinished).will.beTruthy();
    expect(rewardsOp.generatedErrors.count).equal(0);
    expect(rewardsOp.rewards.count).beGreaterThan(0);
    
    NSUInteger min = [[rewardsOp.rewards valueForKeyPath:@"@min.points"] unsignedIntegerValue];
    LTReward *reward = [rewardsOp.rewards filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"points == %d", min]][0];
    
    LTRedemptionSequenceOperation *op = [LTRedemptionSequenceOperation sequenceWithRewardId:reward.rewardId];
    [self addTestOperation:op];

    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
}

- (void)testRefreshAchievementSequenceOperation {
    [self loginAsGuestUser:NO];
    [LTManager sharedManager].data.app.achievements = nil;
    
    expect([LTManager sharedManager].data.app.achievements).to.beNil();
    
    LTRefreshAchievementSequenceOperation *op = [LTRefreshAchievementSequenceOperation sequence];
    [self addTestOperation:op];
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
    expect([LTManager sharedManager].data.app.achievements).notTo.beNil();
}

- (void)testRefreshAchievementSequenceOperationHavingApp {
    [self loginAsGuestUser:YES];
    
    expect([LTManager sharedManager].data.app.achievements).to.beNil();
    
    LTAppOperation *appOp = [LTAppOperation GET];
    LTBlockOperation *blockOp = [LTBlockOperation autoSuccessOpWithBlock:^{ [LTManager sharedManager].data.app = appOp.app; }];
    LTRefreshAchievementSequenceOperation *op = [LTRefreshAchievementSequenceOperation sequence];
    
    [blockOp addDependency:appOp];
    [op addDependency:blockOp];
    
    [self addTestOperation:appOp];
    [self addTestOperation:blockOp];
    [self addTestOperation:op];
    
    expect(appOp.isFinished).will.beTruthy();
    expect(appOp.generatedErrors.count).equal(0);
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);

    expect([LTManager sharedManager].data.app.achievements).notTo.beNil();
}

- (void)testRefreshRewardsSequenceOperation {
    [self loginAsGuestUser:YES];
    
    expect([LTManager sharedManager].data.rewards).to.beNil();
    
    LTRefreshRewardsSequenceOperation *op = [LTRefreshRewardsSequenceOperation sequence];
    [self addTestOperation:op];
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);

    expect([LTManager sharedManager].data.rewards).notTo.beNil();
}

- (void)testRefreshUserAccountSequenceOperation {
   [self loginAsGuestUser:YES];
    
    expect([LTManager sharedManager].data.user).to.beNil();
    
    LTRefreshUserAccountSequenceOperation *op = [LTRefreshUserAccountSequenceOperation sequence];
    [self addTestOperation:op];
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);

    expect([LTManager sharedManager].data.user).notTo.beNil();
}

- (void)testLoginAndAccountSequenceOperation {
    [self loginAsGuestUser:YES];
    [LTManager sharedManager].data.userSessionToken = nil;
    [LTManager sharedManager].data.user = nil;
    
    expect([LTManager sharedManager].data.user).to.beNil();
    
    LTUserSessionOperation *guestOp = [LTUserSessionOperation POSTAsGuest];
    LTBlockOperation *blockOp = [LTBlockOperation autoSuccessOpWithBlock:^{ [LTManager sharedManager].data.userSessionToken = guestOp.userSessionToken; }];
    LTLoginAndAccountSequenceOperation *op = [LTLoginAndAccountSequenceOperation sequenceWithEmail:LTUserEmailTest];

    [blockOp addDependency:guestOp];
    [op addDependency:blockOp];
    
    [self addTestOperation:guestOp];
    [self addTestOperation:blockOp];
    [self addTestOperation:op];
    
    expect(guestOp.isFinished).will.beTruthy();
    expect(guestOp.generatedErrors.count).equal(0);

    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
    
    expect([LTManager sharedManager].data.user.email).equal(LTUserEmailTest);
}

- (void)testLogoutUserSequenceOperation {
    [self loginAsGuestUser:YES];
    [LTManager sharedManager].data.userSessionToken = nil;
    [LTManager sharedManager].data.user = nil;
    
    expect([LTManager sharedManager].data.user).to.beNil();

    LTUserSessionOperation *guestOp = [LTUserSessionOperation POSTAsGuest];
    LTBlockOperation *blockOp = [LTBlockOperation autoSuccessOpWithBlock:^{ [LTManager sharedManager].data.userSessionToken = guestOp.userSessionToken; }];
    LTLogoutUserSequenceOperation *op = [LTLogoutUserSequenceOperation sequence];
    
    [blockOp addDependency:guestOp];
    [op addDependency:blockOp];
    
    [self addTestOperation:guestOp];
    [self addTestOperation:blockOp];
    [self addTestOperation:op];
    
    expect(guestOp.isFinished).will.beTruthy();
    expect(guestOp.generatedErrors.count).equal(0);
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
    
    expect([LTManager sharedManager].data.userSessionToken).toNot.beNil();
    expect([LTManager sharedManager].data.userSessionToken).notTo.equal(guestOp.userSessionToken);
}

- (void)testUpdateUserAccountSequenceOperation {
    [self loginAsUserWithEmail:LTUserEmailTest isNeedResetData:NO];
    //expect([LTManager sharedManager].data.user).to.beNil();
    
    LTUpdateUserAccountSequenceOperation *op = [LTUpdateUserAccountSequenceOperation sequenceWithUserAccount:[self userAccountForTest]];
    [self addTestOperation:op];
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);

    expect([LTManager sharedManager].data.user).notTo.beNil();
}

- (void)testInterstitialWithOfferID {
    [self loginAsGuestUser:YES];
    
    LTVideoInterstitialSequenceOperation *op = [LTVideoInterstitialSequenceOperation sequenceWithOfferID:LTOfferIDTest];
    [self addTestOperation:op];
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
}

- (void)testInterstitialWithAchievmentID {
    [self loginAsGuestUser:YES];
    
    LTVideoInterstitialSequenceOperation *op = [LTVideoInterstitialSequenceOperation sequenceWithAchivmentID:LTAchievementIdTest];
    [self addTestOperation:op];
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
}

#pragma mark -

- (void)loginAsGuestUser:(BOOL)isNeedResetAppData {
    [self loginAsUserWithEmail:nil isNeedResetData:isNeedResetAppData];
}

- (void)loginAsUserWithEmail:(NSString *)email isNeedResetData:(BOOL)isNeedResetAppData {
    [LTManager sharedManager].data.appSecret = nil;
    [LTManager sharedManager].data.apiSessionToken = nil;
    [LTManager sharedManager].data.userSessionToken = nil;
    
    LTInitSequenceOperation *op;
    if (email != nil) {
        op = [LTInitSequenceOperation sequenceWithAppSecret:[LTHelperUnitTest LTAppSecretTest] emailIfNotLogged:email sendLocation:NO];
    }
    else {
        op = [LTInitSequenceOperation sequenceAsGuestWithAppSecret:[LTHelperUnitTest LTAppSecretTest] sendLocation:NO];
    }
    [self addTestOperation:op];
    
    expect(op.isFinished).will.beTruthy();
    expect([LTManager sharedManager].data.appSecret).notTo.beNil();
    expect([LTManager sharedManager].data.apiSessionToken).notTo.beNil();
    expect([LTManager sharedManager].data.userSessionToken).notTo.beNil();
    if (isNeedResetAppData) {
        [LTManager sharedManager].data.app = nil;
    }
    expect(op.generatedErrors.count).equal(0);
}

- (LTUserAccount *)userAccountForTest {
    LTUserAccount *testAccount = [LTUserAccount new];
    testAccount.firstName = @"TestFirstName";
    testAccount.lastName = @"TestLastName";
    testAccount.gender = @"M";
    testAccount.birthdate = [[LTTools dateFormatterWithDateFormat:LTAPIDateOnlyFormat] dateFromString:@"1990-01-01"];
    testAccount.address = @"000 Test address";
    testAccount.city = @"Los Angeles";
    testAccount.state = @"CA";
    testAccount.zipcode = 90000;
    testAccount.confirmedAge = 20;
    testAccount.acceptedTOS = NO;
    testAccount.lootsieOptin = YES;
    testAccount.partnerOptin = NO;
    testAccount.email = LTUserEmailTest;
    
    return testAccount;
}

@end

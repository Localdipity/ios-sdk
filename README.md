# Lootsie iOS SDK version 5.0. Integration Guide for Application Developers

Lootsie is a mobile rewards platform.
In this guide I'll walk you through a basic integration with our platform.

## Overview

Lootsie iOS SDK version 5.0 has a modular structure designed to make it easily configurable, extensible and customizable.

Lootsie service is actively developed and will provide plenty of new features soon. The SDK will grow with a new modules but user will always have choice to link only those used in their application. 

Currently the SDK structure includes **Common** module, **Achievements**, **Rewards Marketplace**, **Rewarded Video Ad**, **User Account Management** module. 

This document guides through steps for quick start, provides detailed installation and usage guide. 

## Limitations

- Minimum iOS Target: 7.0
- Objective-C or Swift project 

## Quick Start Guide
###### Quick start using Objective-C

Step 1
Create directory for the project. Create podfile there and add content:

```ruby
source 'https://github.com/CocoaPods/Specs.git'
platform :ios, '8.0'
pod 'LTLootsie', '~> 5'
```
Step 2
Run command in the project directory:
```ruby
# pod install
```
###### Quick start using Swift
Step 1
Create directory for the project. Create podfile there and add content:
```ruby
source 'https://github.com/CocoaPods/Specs.git'
platform :ios, '8.0'
use_frameworks!
pod 'LTLootsie', '~> 5'
```
Step 2.
Run command in the project directory:
```ruby
# pod install
```
###### Notes
Rewarded Video Ad module currently configured to get video ads from AdMob network by default.
## The SDK Structure
The SDK includes Common module, Achievements, Rewards Marketplace, Rewarded Video Ad, User Account Management module.
Common module provides main functionality which all other modules depend upon and  also keeps commonly used utility functions.
All modules, except for Common module,  are contained in the LTModule folder. Each module there contains **UI** and **Core** folders:

- Core folder contains the module’s data model and server interaction code. 
- UI folder contains the module’s user interface classes.

UI folder dependent on Core folder. Core folder can’t be used without UI folder for whitelabel developers.

Common module is essential for the project as other modules are depend on it. This module has similar structure, though its Core is in LTLootsie folder and UI is in LTModule folder.

## Installation Guide
###### Automated installation using CocoaPods
Each module contains UI and Core, having the same partition and module dependency 
1.1) **Common module** has the following structure: LTLootsie/Core and LTLootsie/UI.
In order to enable one of the module’s part, it is necessary to write to Podfile:
```ruby
pod LTLootsie/Core 
```
for Core and
```ruby
pod LTLootsie/UI 
```
if there is a need for UI for the current module

1.2) **Achievement module**. This module has the same partition as Common module:
```ruby
 LTLootsie/AchievementCore 
 LTLootsie/AchievementUI.
```
1.3) **Marketplace module**. This module has the same partition as Common module: 
```ruby
LTLootsie/MarketplaceCore
LTLootsie/MarketplaceUI.
```
1.4) **Interstitial module**. This module should include UI of Lootsie and may vary depending on ad SDK.
If **AdMob SDK** should be used, the following data should be added:.
```ruby
pod LTLootsie/AdMob
```
If **Aerserv SDK** to be used, the following should added:
```ruby
pod LTLootsie/Aerserv
```
1.5) **Account module**. This module has the same partition as Common module: LTLootsie/AccountCore and LTLootsie/AccountUI.  
```ruby
LTLootsie/AccountCore
LTLootsie/AccountUI.
```

###### Manual Installation
Add these keys in your info.plist file. (Related to the App Transport Security as of ios 9.0.) They specify the exceptions needed for each domain so your app can successfully load the images for rewards.
```ruby
<key>NSAppTransportSecurity</key>
<dict>
    <key>NSAllowsArbitraryLoads</key>
    <true/>
</dict>
```
And done! Now every time the user opens your app that line of code makes Lootsie SDK inform the API that the current user just got a reward. If you run your app you should see an IAN (In-App Notification) coming down containing all the information about the achievement you just reached and by tapping on it you should be taken to Lootsie's Rewards Page.

Modules enabling.

1) In order to enable the **Common module**, it’s necessary to add source code to project found at https://bitbucket.org/LootsieIOS/ios-sdk

2) In order to enable an **Achievement**, it’s necessary for Common module to be added along with ENABLE_ACHIEVEMENT=1  macros into Build Settings > Preprocessor Macros

https://bitbucket.org/lootsie/ios-sdk-5-achievements

3) In order to enable **Marketplace module**, it’s necessary for Common module to be added along with ENABLE_MARKETPLACE=1  macros into Build Settings > Preprocessor Macros

https://bitbucket.org/lootsie/ios-sdk-5-rewards

4) In order to enable **Interstitial module**, it’s necessary to enable Marketplace and Achievement modules. Also, macros ENABLE_INTERSTITIAL=1 should be added to Build Settings > Preprocessor Macros

https://bitbucket.org/lootsie/ios-sdk-5-video-reward

4.1) In order to include **AdMob** into the project, go to Build Settings > Preprocessor Macros and add USE_ADMOB for all targets. USE_ADMOB is a define that defines for compiler what to add to the project for AdMob usage.

4.2) In order to include **AerServ** into the project, go to Build Settings > Preprocessor Macros and add USE_AERSERV for all targets. USE_AERSERV is a define that defines for compiler what to add to the project for AerServ usage. Also in Xcode set Others Link Flag = -ObjC and add libxml2.2.dylib. 

5) In order to enable **Account module**, it’s necessary for Common module to be added along with ENABLE_ACCOUNT=1  macros into Build Settings > Preprocessor Macros

https://bitbucket.org/lootsie/ios-sdk-5-account

## Usage

Lootsie SDK uses a main manager class to execute most actions you'll need for your app. To grab a reference of the manager, type `[LTUIManager sharedManager]`. We'll use that form from now until the end of the *Usage* section but keep in mind that if you're a white label developer and you're using only the `Core` cocoapod subspec or if you just have copied the folder *LTLootsie* during manual installation you should use `[LTManager sharedManager]` instead to grab a reference of your manager.

### 1. Initialization

To initialize Lootsie SDK you'll need the `APP KEY` that you’ve created before, on the step #Set up a new Application#. Open your `AppDelegate` implementation file and add the following code before returning from `application:didFinishLaunchingWithOptions:`

##### Swift
```swift
let appKey = "YOUR-APP-KEY-HERE"

LTUIManager.sharedManager().startEngineWithAppKey(appKey, success: nil, failure: nil)
```
        
##### Objective-C
```objective-c
NSString *appKey = @"YOUR-APP-KEY-HERE";

[[LTUIManager sharedManager] startEngineWithAppKey:appKey success:nil failure:nil];
```

Observe that this call also receives optional blocks to handle success and failure cases. This is a standard pattern adopted throughout the whole SDK. So, every time you call a method that can potentially access the internet to retrieve the result you asked for, it'll give you the option to pass blocks to handle both success and failure cases.

> This won't trigger any network call yet but it'll save the information necessary for a lazy initialization. If any event that needs Lootsie SDK to be initialized occurs, e.g.: The user reaching an achievement,  it'll trigger the initialization process and then execute the event that just happened.

### 2. Delegate for Interface behavior
If you're using the `LTUIManager` you can conform to a protocol named `LTUIManagerDelegate` to participate on how the interface shows on top of your app and to know when it happens. You can be notified when it's about to appear and as soon as it's been dismissed.

See the example below that demonstrates how to display Lootsie's interface inside a child view controller, how to pause the game when the interface is being shown and how to resume afterwards.

First conform to the protocol:

##### Swift
```swift
class AppDelegate: UIResponder, UIApplicationDelegate, LTUIManagerDelegate
```
##### Objective-C
```objective-c
@interface AppDelegate () <LTUIManagerDelegate>
    // ...
@end
```

Next implement the delegate methods you want:

##### Swift
```swift
func lootsieUIManagerWillShowUI() {
    gameController.pause()
}

func lootsieUIManagerDidCloseUI() {
    gameController.resume()
}

func presentationContextForUIManager() -> UIViewController {
    return childViewController;
}
```

##### Objective-C
```objetive-c
- (void)lootsieUIManagerWillShowUI {
    [self.gameController pause];
}

- (void)lootsieUIManagerDidCloseUI {
    [self.gameController resume];
}

- (UIViewController *)presentationContextForUIManager {
    - return self.childViewController;
- }
```


### 3. Set up a button to show Lootsie Rewards

Here's a small example demonstrating how to set up the action for a button that shows Lootsie's Reward Page.

##### Swift
```swift
@IBAction func showLootsieRewards(sender: AnyObject) {
    LTUIManager.sharedManager().showRewardsPage()
}
```

##### Objective-C
```objective-c
- (IBAction)showLootsieRewards: (id)sender {
    [[LTUIManager sharedInstance] showRewardsPage];
}
```

Tapping the button linked to that IBAction will show Lootsie's Interface on the Rewards Page. Good job! You can examine the available methods on `LTUIManager` class for more interface options.

### 4. Implement Achievements

The last step we’re going to show is how to tell Lootsie's API that your user's reached an achievement so he/she can be rewarded.

To do that we’ll use the achievement you've created earlier with the API ID **launch** on the step **Set up a new Application**. To do that open the `AppDelegate` implementation file again and put the following code right after the Lootsie’s initialization code.

##### Swift
```swift
LTUIManager.sharedManager().reportAchievementWithId("launch", success: nil, failure: nil)
```

##### Objective-C
```objective-c
[[LTUIManager sharedManager] reportAchievementWithId:@"launch" success:nil failure:nil];
```

And done! Now every time the user opens your app that line of code makes Lootsie SDK inform the API that the current user just got a reward. If you run your app you should see an IAN (In-App Notification) coming down containing all the information about the achievement you just reached and by tapping on it you should be taken to Lootsie's Rewards Page.

## Where to go from here?

After going through this basic integration you can explore our SDK by opening `LTUIManager` and `LTManager` classes and seeing what's being offered by default in terms of features. You should also find some documentation in the code itself, describing method calls and parameters. Here are some highlights of what you can do:

* Update user account
* Change IAN default behavior
* Force a specific orientation
* Logout
* Disable lazy initialization

## License

LTLootsie is released under the MIT license. See LICENSE for details.
